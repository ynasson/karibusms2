package karibusms.dashboard;

/**
 * Created by joshuajohn on 01/09/16.
 */
public class Dashboard {

    private int dashboardImage;
    private String dashboardName;


    public Dashboard(int dashboardImage,String dashboardName){
        this.dashboardName=dashboardName;
        this.dashboardImage=dashboardImage;

    }

    public int getDashboardImage() {
        return dashboardImage;
    }

    public void setDashboardImage(int dashboardImage) {
        this.dashboardImage = dashboardImage;
    }

    public String getDashboardName() {
        return dashboardName;
    }

    public void setDashboardName(String dashboardName) {
        this.dashboardName = dashboardName;
    }
}
