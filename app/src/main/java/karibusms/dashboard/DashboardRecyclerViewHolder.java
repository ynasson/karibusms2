package karibusms.dashboard;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import karibusms.R;
import karibusms.activities.StatisticActivity;
import karibusms.category.ViewCategory;
import karibusms.contacts.ContactActivity;
import karibusms.activities.FaqActivity;
import karibusms.activities.SendingSMS;


public class DashboardRecyclerViewHolder extends RecyclerView.ViewHolder  implements OnClickListener{

    public  TextView dashboardName;
    public  ImageView dashboardImage;
    private Context context;

    public DashboardRecyclerViewHolder(View itemView) {
        super(itemView);
        context=itemView.getContext();
        itemView.setOnClickListener(this);
        dashboardName=(TextView)itemView.findViewById(R.id.dashboard_name);
        dashboardImage=(ImageView)itemView.findViewById(R.id.dashboard_image);
    }

    @Override
    public void onClick(View v) {

          final Intent intent;
          switch (getPosition()){
              case 0:
                  intent=new Intent(context, SendingSMS.class);
                  context.startActivity(intent);
                  break;

              case 1:
                  intent=new Intent(context, ContactActivity.class);
                  context.startActivity(intent);
                  break;
              case 2:
                  intent=new Intent(context, StatisticActivity.class);
                  context.startActivity(intent);
                  break;

              case 3:
                  intent=new Intent(context, ViewCategory.class);
                  context.startActivity(intent);
                  break;
              case 4:
                  intent=new Intent(context, FaqActivity.class);
                  context.startActivity(intent);
                  break;

          }

    }


}
