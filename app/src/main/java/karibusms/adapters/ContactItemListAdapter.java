package karibusms.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import karibusms.R;

/**
 * Created by joshuajohn on 09/09/16.
 */
public class ContactItemListAdapter extends ArrayAdapter<String> {

    private final Activity activity;
    private final String[] items;


    public ContactItemListAdapter( Activity activity, String[] items) {
        super(activity, R.layout.about_us_list_items,items);
        this.activity = activity;
        this.items = items;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=activity.getLayoutInflater();
        View view=inflater.inflate(R.layout.about_us_list_items,null,true);
        TextView tv_name=(TextView)view.findViewById(R.id.item_name);
        tv_name.setText(items[position]);

        // Font path
        String fontPath = "fonts/Lato-Regular.ttf";

        // Loading Font Face
        Typeface tf = Typeface.createFromAsset(activity.getAssets(), fontPath);
        tv_name.setTypeface(tf);
        return view;
    }
}
