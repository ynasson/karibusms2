package karibusms.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import karibusms.fragments.SendSMS;
import karibusms.fragments.ReceivedSMS;
import karibusms.fragments.SendSingleMessage;
import karibusms.fragments.sentsms;


public class ViewPagerAdapter extends FragmentPagerAdapter {
    int numTab;

    public ViewPagerAdapter(FragmentManager fm ,int numTab) {
        super(fm);
        this.numTab = numTab;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                SendSMS sendsms = new SendSMS();
                return sendsms;
            case 1:
                SendSingleMessage sms_single = new SendSingleMessage();
                return sms_single;
//            case 2:
//                ReceivedSMS recsms = new ReceivedSMS();
//                return recsms;
            case 2:
                sentsms sms = new sentsms();
                return sms;



            default:
                return null;
        }
    }

    @Override
    public int getCount() {
      return numTab;
    }
}
