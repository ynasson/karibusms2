package karibusms.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import karibusms.info.SmsList;
import karibusms.logging.L;

/**
 * Created by Yohana on 4/22/2016.
 */
public class SmsStore  {
    private KaribusmsdbHelper mHelper;
    private SQLiteDatabase nDatabase;
    public SmsStore(Context contex) {
        mHelper = new KaribusmsdbHelper(contex);
        nDatabase = mHelper.getWritableDatabase();
    }


    public void insertPhones( ArrayList<String> listPhones,String msg,String link, boolean
            clearPrevious,String
            table) {
        Log.d("LISTPHONES",listPhones.toString());

        if (clearPrevious) {
            deletePhones();
        }
        //create a sql prepared statement
        String sql = "INSERT INTO " + (table) + " VALUES (?,?,?,?,?);";
        //compile the statement and start a transaction
        Log.d("KARIBUSMS", sql);
        SQLiteStatement statement = nDatabase.compileStatement(sql);
        try {
            nDatabase.beginTransaction();
            for (int i = 0; i < listPhones.size(); i++) {

                statement.clearBindings();
                String phone = listPhones.get(i);
                //for a given column index, simply bind the data to be put inside that index
                statement.bindString(2, phone);
                statement.bindString(3, msg);
                statement.bindString(4, link);
                statement.bindString(5, "0");
                statement.execute();
            }
            nDatabase.setTransactionSuccessful();
        } catch (Exception e) {
            L.m(e.toString());
        } finally {
            nDatabase.endTransaction();
        }


    }
    public void deletePhones() {

        nDatabase.delete(KaribusmsdbHelper.TABLE_KARIBUSMS, null, null);
    }
    public void deleteFromlist(Integer id) {
        String[] column = {
                //status=0;
                id.toString()
        };

        nDatabase.delete(KaribusmsdbHelper.TABLE_KARIBUSMS, "id=?", column);
       // L.m("PHONE DELETED");
    }
    public ArrayList<SmsList> perseSmsinfo() {
        ArrayList<SmsList> numbers=new ArrayList<>();

        //get a content of columns to be retrieved
        String[] columns = {KaribusmsdbHelper.COLUMN_UID,
                KaribusmsdbHelper.COLUMN_PHONE,
                KaribusmsdbHelper.COLUMN_MESSAGE,
                KaribusmsdbHelper.COLUMN_LINK,
        };
        String[] column = {
                //status=0;
                "0"
        };
        // nDatabase.
        Cursor cursor = nDatabase.query((KaribusmsdbHelper.TABLE_KARIBUSMS),columns, "status=?",
                column,null,null,
                null);
        if (cursor != null && cursor.moveToFirst()) {

            do {
                SmsList smsphonelist=new SmsList();
                Integer  phone_id= cursor.getInt(cursor.getColumnIndex(KaribusmsdbHelper
                        .COLUMN_UID));
                String  phone_number= cursor.getString(cursor.getColumnIndex(KaribusmsdbHelper
                        .COLUMN_PHONE));
                String  phone_msg= cursor.getString(cursor.getColumnIndex(KaribusmsdbHelper
                        .COLUMN_MESSAGE));
                String  phone_link= cursor.getString(cursor.getColumnIndex(KaribusmsdbHelper
                        .COLUMN_LINK));
                smsphonelist.setSid(phone_id);
                smsphonelist.setPhone(phone_number);
                smsphonelist.setLink(phone_link);
                smsphonelist.setMsg(phone_msg);
                numbers.add(smsphonelist);
            } while (cursor.moveToNext());
        }
        return numbers;
    }

    public static class KaribusmsdbHelper extends SQLiteOpenHelper {
        public static final String TABLE_KARIBUSMS = "karibusms";
        public static final String COLUMN_UID = "id";
        public static final String COLUMN_PHONE = "phonenumber";
        public static final String COLUMN_MESSAGE = "message";
        public static final String COLUMN_STATUS = "status";
        public static final String COLUMN_LINK = "msg_link";
        private static final String CREATE_TABLE_KARIBUSMS = "CREATE TABLE " + TABLE_KARIBUSMS + 
                " (" +
                COLUMN_UID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_PHONE + " TEXT," +
                COLUMN_MESSAGE + " TEXT," +
                COLUMN_LINK + " TEXT," +
                COLUMN_STATUS + " VARCHAR" +
                ");";


        //create another table here
        private static final String DB_NAME = "karibusms_db";
        private static final int DB_VERSION = 1;
        private Context mContext;

        public KaribusmsdbHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
            mContext = context;
          // Toast.makeText(mContext,
                  // "constructor called", Toast.LENGTH_SHORT)
                  //  .show();
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(CREATE_TABLE_KARIBUSMS);
              //  L.m("create table KARIBUSMS  executed");
            } catch (SQLiteException exception) {
               // Log.d("CREATE TABLE", exception + "");
               //  L.T(mContext, exception+"");
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            try {
                db.execSQL(" DROP TABLE " + TABLE_KARIBUSMS + " IF EXISTS;");
                onCreate(db);
            } catch (SQLiteException exception) {
               // L.t(mContext, exception + "");
            }
        }
    }
}

