package karibusms.category;

/**
 * Created by joshuajohn on 12/10/2016.
 */

public class Category {

    private String categoryId;
    private String categoryName;
    private int id;


    public  Category(){

    }
    public Category(int id,String categoryName){
        this.id=id;
        this.categoryName=categoryName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
