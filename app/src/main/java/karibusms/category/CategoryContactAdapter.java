package karibusms.category;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.List;

import karibusms.R;
import karibusms.contacts.Contact;


public class CategoryContactAdapter extends BaseAdapter {
    private List<Contact> contactList;
    private LayoutInflater inflater;
    private Activity activity;

    public  CategoryContactAdapter(List<Contact> categoryList,Activity activity){
        this.contactList=categoryList;
        this.activity=activity;
    }
    @Override
    public int getCount() {
        return contactList.size();
    }

    @Override
    public Object getItem(int position) {
        return contactList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            // inflate UI from XML file
            convertView = inflater.inflate(R.layout.category_contact_list_items, parent, false);

        TextView categoryName=(TextView)convertView.findViewById(R.id.category_contact_name);
        TextView phone=(TextView)convertView.findViewById(R.id.category_contact_phone_number);
        ImageView imageView=(ImageView)convertView.findViewById(R.id.category_contact_image_view);

        Contact contact=contactList.get(position);
        categoryName.setText(contact.getContactName());
        phone.setText(contact.getPhone_number());

        char cat_name=contact.getContactName().charAt(0);
        String name=String.valueOf(cat_name);

        ColorGenerator generator = ColorGenerator.MATERIAL;

        // generate random color
        int color = generator.getColor(getItem(position));
        //int color = generator.getRandomColor();

        TextDrawable drawable = TextDrawable.builder()
                .buildRound(name, color); // radius in px

        imageView.setImageDrawable(drawable);
        return convertView;
    }
}
