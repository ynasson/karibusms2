package karibusms.category;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.List;

import karibusms.R;

/**
 * Created by joshuajohn on 11/10/2016.
 */

public class CategoryAdapter extends BaseAdapter {
    private List<Category> categoryList;
    private LayoutInflater inflater;
    private Activity activity;

    public  CategoryAdapter(List<Category> categoryList,Activity activity){
        this.categoryList=categoryList;
        this.activity=activity;
    }
    public CategoryAdapter(){

    }
    @Override
    public int getCount() {
        return categoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return categoryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            // inflate UI from XML file
            convertView = inflater.inflate(R.layout.list_items_category, parent, false);

        TextView categoryName=(TextView)convertView.findViewById(R.id.category_name);
        ImageView imageView=(ImageView)convertView.findViewById(R.id.category_image_view);

        Category category=categoryList.get(position);
        categoryName.setText(category.getCategoryName());

        char cat_name=category.getCategoryName().charAt(0);
        String name=String.valueOf(cat_name);

        ColorGenerator generator = ColorGenerator.MATERIAL;

        // generate random color
        int color = generator.getColor(getItem(position));
        //int color = generator.getRandomColor();

        TextDrawable drawable = TextDrawable.builder()
                .buildRound(name, color); // radius in px

        imageView.setImageDrawable(drawable);
        return convertView;
    }
}
