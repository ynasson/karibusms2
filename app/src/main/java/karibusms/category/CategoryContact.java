package karibusms.category;

import android.app.ProgressDialog;

import android.content.DialogInterface;
import android.content.Intent;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import android.view.MenuItem;

import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import karibusms.R;
import karibusms.contacts.Contact;

import karibusms.extras.AppConfig;
import karibusms.extras.ErrorMessage;
import karibusms.extras.Utils;
import karibusms.network.VolleySingleton;

public class CategoryContact extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    private Intent intent;
    private ProgressDialog dialog,deleteDialog;
    private String  category_id,business_id;
    private List<Contact>contactList=new ArrayList<>();
    private ErrorMessage errormsg;
    private RequestQueue requestQueue;
    private ListView catContactsList;
    private Button deleteCategoryBtn;
    String messageSuccess;
    private CategoryContactAdapter categoryContactAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_contacts);
        deleteCategoryBtn=(Button)findViewById(R.id.delete_category_btn);
        deleteCategoryBtn.setOnClickListener(this);
        requestQueue= Volley.newRequestQueue(this);
        deleteDialog=new ProgressDialog(this);
        deleteDialog.setMessage("Deleting Category...");
        deleteDialog.setIndeterminate(false);

        dialog=new ProgressDialog(this);
        dialog.setMessage("Loading Contacts...");

        catContactsList=(ListView)findViewById(R.id.category_contacts_list) ;
        categoryContactAdapter=new CategoryContactAdapter(contactList,this);
        catContactsList.setAdapter(categoryContactAdapter);
        initializeView();
        getContactByCategory();
    }

    private void initializeView(){
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void getContactByCategory(){

        dialog.show();
        intent=getIntent();
        if(intent!=null){
            category_id=intent.getStringExtra("category_id");

        }
        business_id=Utils.readPreferences(CategoryContact.this,"business_id","");

        String getContactByCategoryUrl= AppConfig.getContactByCategory("getContactsByCategory",business_id,category_id);
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(getContactByCategoryUrl, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                if (dialog.isShowing()){
                    dialog.dismiss();
                }

                for(int i=0; i<response.length(); i++){

                    try {
                        JSONObject jsonObject=response.getJSONObject(i);
                        Contact contact=new Contact();
                        String catName=jsonObject.getString("category");

                        toolbar.setTitle(catName);

                        if (jsonObject.getString("phone_number").equalsIgnoreCase("null")
                                || jsonObject.getString("phone_number").isEmpty()){
                            contact.setContactName("No phone Number");
                        }
                        else{
                            contact.setPhone_number(jsonObject.getString("phone_number"));

                        }
                        if(jsonObject.getString("firstname").equalsIgnoreCase("null") ||
                                jsonObject.getString("firstname").isEmpty()){
                            contact.setContactName("No Name");

                        }else{
                            contact.setContactName(jsonObject.getString("firstname"));
                        }
                        contactList.add(contact);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                categoryContactAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }

        });
        VolleySingleton.getInstance().getRequestQueue().add(jsonArrayRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main,menu);
        MenuItem item=menu.findItem(R.id.add_category);
        item.setVisible(false);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, ViewCategory.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;

            default:
                return false;
        }
        return super.onOptionsItemSelected(item);
    }


    private void deleteCategory() {
        deleteDialog.show();
        Intent i = getIntent();


        category_id = i.getStringExtra("category_id");
        business_id = Utils.readPreferences(CategoryContact.this, "business_id", "");
        String urlDeleteCat = AppConfig.deleteCategoryUrl("deleteCategory", "category", category_id, business_id);

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, urlDeleteCat, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                deleteDialog.dismiss();
                try {
                    if (response.getString("status").equalsIgnoreCase("success")) {
                        messageSuccess=response.getString("message");
                        printMsg(messageSuccess);


                    }
                }catch (JSONException e){
                    e.printStackTrace();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        VolleySingleton.getInstance().getRequestQueue().add(jsonObjectRequest);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.delete_category_btn:
                deleteCategoryDialog();
//                Intent intent = new Intent(this, ViewCategory.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
//                finish();

                break;
        }

    }

    public void deleteCategoryDialog(){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setMessage("Are you deleting category?By deleting category ,contacts will not be deleted.");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteCategory();

            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });
        AlertDialog d=builder.create();
        d.show();

    }

    public  void printMsg(String msg) {
        new AlertDialog.Builder(this)
                .setTitle("Info")
                .setMessage(msg)
                .setIcon(R.drawable.ic_attachment)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }
}
