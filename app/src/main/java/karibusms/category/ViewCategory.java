package karibusms.category;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import karibusms.R;
import karibusms.extras.AppConfig;
import karibusms.extras.ErrorMessage;
import karibusms.extras.Utils;
import karibusms.network.VolleySingleton;

public class ViewCategory extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ListView listCategory;
    private List<Category> listOfCategories = new ArrayList<>();
    private CategoryAdapter categoryAdapter;
    private ProgressDialog progressDialog, createDialog;
    private EditText etCategoryName;
    private ErrorMessage errorMessage;
    private RequestQueue requestQueue;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_category);
        setToolBar();

        requestQueue = Volley.newRequestQueue(this);

        progressDialog = new ProgressDialog(this);
        createDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading Category");
        createDialog.setMessage("Creating Category");
        progressDialog.setIndeterminate(false);
        createDialog.setIndeterminate(false);

        listCategory = (ListView) findViewById(R.id.category_list);
        categoryAdapter = new CategoryAdapter(listOfCategories, this);
        listCategory.setAdapter(categoryAdapter);
        listCategory.setOnItemClickListener(this);
        getCategories();

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                listOfCategories.clear();
                getCategories();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void setToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void getCategories() {
        progressDialog.show();
        String business_id = Utils.readPreferences(ViewCategory.this, "business_id", "");
        String getCategoryUrl = AppConfig.getCategoriesUrl("getCategory", business_id);
        Log.v("URL", getCategoryUrl.toString());

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(getCategoryUrl, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                progressDialog.dismiss();
                Log.v("MY DATA", response.toString());
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject jsonObject = response.getJSONObject(i);
                        Category category = new Category();
                        category.setCategoryName(jsonObject.getString("category_name"));
                        category.setCategoryId(jsonObject.getString("category_id"));
                        listOfCategories.add(category);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                swipeRefreshLayout.setRefreshing(false);
                categoryAdapter.notifyDataSetChanged();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        VolleySingleton.getInstance().getRequestQueue().add(jsonArrayRequest);
    }

    private void addCategoryDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.add_new_category, null);
        etCategoryName = (EditText) dialogView.findViewById(R.id.categoryName);
        builder.setTitle("New Contact Category");
        builder.setView(dialogView);

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setPositiveButton("ADD", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                createDialog.show();
                String nameCategory = etCategoryName.getText().toString().trim();
                String business_id = Utils.readPreferences(ViewCategory.this, "business_id", "");

                if (nameCategory.equalsIgnoreCase("")) {
                    etCategoryName.setError("Category  name is required");
                }
                String createGroupUrl = AppConfig.createNewGroupUrl("createCategory", nameCategory, business_id);
                httpRequest(createGroupUrl);
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.add_category:
                addCategoryDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void httpRequest(String url) {

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                createDialog.dismiss();
                parseData(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                errorMessage.handleVolleyError(error);
            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    private void parseData(JSONObject response) {
        String messageSuccess;
        if (response != null && response.length() > 0) {
            try {
                if (response.getString("status").equalsIgnoreCase("success")) {
                    messageSuccess = response.getString("message");
                    printMsg(messageSuccess);
                    etCategoryName.setText("");
                } else {
                    String msg = response.getString("message");
                    printMsg(msg);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void printMsg(String msg) {
        new AlertDialog.Builder(this)
                .setTitle("FeedBack Info")
                .setMessage(msg)
                .setIcon(R.drawable.ic_attachment)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Category category = (Category) categoryAdapter.getItem(position);
        Intent intent = new Intent(ViewCategory.this, CategoryContact.class);
        intent.putExtra("category_id", category.getCategoryId());
        startActivity(intent);
        finish();
    }


}
