package karibusms.callback;
import java.util.ArrayList;

import karibusms.info.SmsList;

/**
 * Created by Yohana on 2/19/2016.
 */
public interface ResultsLoadedListener {
     void onResultsloadedListener(ArrayList<SmsList> listphone);
}
