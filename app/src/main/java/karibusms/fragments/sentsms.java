package karibusms.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;



import karibusms.R;
import karibusms.activities.Detail;
import karibusms.adapters.SentSMSAdapter;
import karibusms.category.ViewCategory;
import karibusms.extras.AppConfig;
import karibusms.extras.ErrorMessage;
import karibusms.extras.PrintMessage;
import karibusms.extras.Utils;
import karibusms.info.SMSSent;
import karibusms.network.VolleySingleton;


public class sentsms extends Fragment implements SwipeRefreshLayout.OnRefreshListener,AdapterView.OnItemClickListener {

    private List<SMSSent> sentSMSList=new ArrayList<SMSSent>();
    private SentSMSAdapter sentSMSAdapter;
    private ListView listSentSMS;
    private View view;
    private ErrorMessage errormsg;
    private RequestQueue requestQueue;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressDialog progressDialog,dialog;

    public sentsms() {

        }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_sentsms, container, false);
        requestQueue= Volley.newRequestQueue(getActivity());
        progressDialog=new ProgressDialog(getActivity());
        dialog=new ProgressDialog(getActivity());
        dialog.setMessage("Deleting SMS...");
        dialog.setIndeterminate(false);

        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(false);


        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        listSentSMS = (ListView) view.findViewById(R.id.sent_sms_list);
        sentSMSAdapter = new SentSMSAdapter(getActivity(), sentSMSList);
        listSentSMS.setAdapter(sentSMSAdapter);

        getSentSMS();
        swipeRefreshLayout.setOnRefreshListener(this);
        listSentSMS.setOnItemClickListener(this);
        return view;

    }
    private void getSentSMS(){
        progressDialog.show();
        String business_id= Utils.readPreferences(getActivity(),"business_id","");
        String url= AppConfig.sentSMSUrl("sent_sms",business_id);

        final JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if(progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
               for(int i=0; i<response.length(); i++){

                   try {
                       JSONObject jsonObject=response.getJSONObject(i);
                       SMSSent sms=new SMSSent();

                       if(jsonObject.getString("message_id").equalsIgnoreCase("null")){
                           sms.setSmsId("Null");
                       }else{
                           sms.setSmsId(jsonObject.getString("message_id"));
                       }


                       if(jsonObject.getString("message").equalsIgnoreCase("null")){
                           sms.setMessage("No Message");
                       }
                       else{

                           sms.setMessage(jsonObject.getString("message"));
                       }

                       if(jsonObject.getString("sms_count").equalsIgnoreCase("null")){
                           sms.setNoPeople("0");
                       }else {
                           sms.setNoPeople(jsonObject.getString("sms_count"));
                       }

                       if (jsonObject.getString("category_name").equalsIgnoreCase("null")){
                           sms.setCategory("All");
                       }
                       else{
                           sms.setCategory(jsonObject.getString("category_name"));
                       }

                       sms.setTime(jsonObject.getString("time"));

                       sentSMSList.add(sms);


                   } catch (JSONException e) {
                       e.printStackTrace();
                   }
               }
                sentSMSAdapter.notifyDataSetChanged();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(progressDialog.isShowing()){
                    progressDialog.dismiss();
                }

            }

        });
        VolleySingleton.getInstance().getRequestQueue().add(jsonArrayRequest);
    }


    @Override
    public void onRefresh() {
        sentSMSList.clear();
        getSentSMS();
        swipeRefreshLayout.setRefreshing(false);

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        SMSSent sms = (SMSSent) sentSMSAdapter.getItem(position);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getLayoutInflater(getArguments());
        final View dialogView = inflater.inflate(R.layout.view_sent_sms, null);
        TextView tvSmsBody = (TextView) dialogView.findViewById(R.id.sms_body);
        TextView tvCategoryName = (TextView) dialogView.findViewById(R.id.category_name);
        TextView tvTime = (TextView) dialogView.findViewById(R.id.time_of_send);
        TextView tvContactNumber = (TextView) dialogView.findViewById(R.id.contacts_number);

        String smsBody=sms.getMessage();
        String catName=sms.getCategory();
        String time=sms.getTime();
        String contacts=sms.getNoPeople();

        String messageId=sms.getSmsId();
        String business_id=Utils.readPreferences(getActivity(),"business_id","");

        final String deleteUrl= AppConfig.sendDeleteMessageUrl("deleteMessage",business_id,messageId);


        tvSmsBody.setText("SMS:\n"+smsBody);
        tvCategoryName.setText("Category:\n"+catName);
        tvTime.setText("Time Sent:\n"+time);
        tvContactNumber.setText("Number of Contacts:\n"+contacts);


        builder.setTitle("SMS Detail");
        builder.setView(dialogView);

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });
        builder.setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteDialog(deleteUrl);
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();


    }

    private void parseData(JSONObject response){

        String messageSuccess;
        if(response != null && response.length() > 0){
            dialog.dismiss();
            try{
                if (response.getString("status").equalsIgnoreCase("success")){
                    messageSuccess=response.getString("message");
                    printMsg(messageSuccess);

                }else{
                    String msg=response.getString("message");
                    printMsg(msg);
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    }



    private void httpRequest(String url){
        dialog.show();
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                dialog.dismiss();
                parseData(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                errormsg=new ErrorMessage(getActivity());
                errormsg.handleVolleyError(error);
            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    public void deleteDialog(final String url){
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you deleting this message?");
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                httpRequest(url);

            }
        });
        AlertDialog dialog=builder.create();
        dialog.show();
    }

    public  void printMsg(String msg) {
        new AlertDialog.Builder(getActivity())
                .setTitle("Info")
                .setMessage(msg)
                .setIcon(R.drawable.ic_attachment)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }

}
