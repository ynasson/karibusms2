package karibusms.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;
import java.util.List;

import karibusms.R;

import karibusms.dashboard.Dashboard;

import karibusms.dashboard.DashboardRecyclerViewAdapter;
import karibusms.extras.SessionManager;

/**
 * Created by joshuajohn on 02/09/16.
 */
public class Home extends Fragment {
    View view;
    private RecyclerView recyclerView;
    private SessionManager session;
    private Toolbar toolbar;
    private GridLayoutManager gridLayoutManager;
    private DashboardRecyclerViewAdapter dashboardRecyclerViewAdapter;

    private List<Dashboard> items;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.home, container, false);
        session=new SessionManager(getActivity());
        initializeViews();

        return view;
    }

    public Home(){

    }

        private void initializeViews(){
        List<Dashboard> dashItems=getDashboard();
        gridLayoutManager=new GridLayoutManager(getActivity(),2);
        recyclerView=(RecyclerView)view.findViewById(R.id.dashboard_recycler_view);
        recyclerView.setLayoutManager(gridLayoutManager);
            
        dashboardRecyclerViewAdapter=new DashboardRecyclerViewAdapter (dashItems,getActivity());
        recyclerView.setAdapter(dashboardRecyclerViewAdapter);

    }

        private  List<Dashboard> getDashboard(){
        items=new ArrayList<>();
            items.add(new Dashboard(R.drawable.sms,"MESSAGING"));
            items.add(new Dashboard(R.drawable.web,"CONTACTS"));
            items.add(new Dashboard(R.drawable.home,"STATISTICS"));
            items.add(new Dashboard(R.drawable.group,"CONTACT CATEGORY"));
            items.add(new Dashboard(R.drawable.faqs,"FAQ"));

        return items;

    }


}
