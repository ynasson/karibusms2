package karibusms.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import karibusms.R;
import karibusms.adapters.ContactItemListAdapter;
import karibusms.extras.AppConfig;
import karibusms.extras.ErrorMessage;
import karibusms.logging.L;
import karibusms.network.VolleySingleton;

/**
 * Created by INETS COMPANY LIMITED on 02/09/16.
 * Address Mikocheni B, Bima Road, Block no 11,Dar es salaam, Tanzania
 * +255 655 406 004 or +255 22 278 0228
 */

public class About extends Fragment {
    private View view;
    private ListView list;
    private String[] items = {
            "Mobile Call\n+255 655 406 004 ",
            "Email Us\ninfo@karibusms.com",
            "Telephone Call\n+255 22 278 0228",
            "Visit Our Website\nwww.karibusms.com",
            "Feedback",
            "Terms and Privacy",
            "Like Us on Facebook",
            "Follow Us on twitter",
            "Rate Us in Google Play",
            "Share this App"};

    private ImageLoader imageLoader;
    private VolleySingleton mVolleySingleton;
    private RequestQueue requestQueue;
    private ErrorMessage errormsg;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.about, container, false);
        getAboutItems();

        MyPhoneListener phoneListener = new MyPhoneListener();
        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);
        return view;

    }

    private void getAboutItems() {
        ContactItemListAdapter contactItemListAdapter = new ContactItemListAdapter(getActivity(), items);
        list = (ListView) view.findViewById(R.id.list_items);
        list.setAdapter(contactItemListAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                    try {
                        String uri = "tel:+255655406004";
                        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(uri));
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        startActivity(callIntent);
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), "Your call has failed...",
                                Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                    break;

                    case 1:
                    final Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setType("text/plain");
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@karibusms.com"});
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Greetings");
                    emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    break;

                    case 2:
                    try {
                        //set the data
                        String uri = "tel:+255222780228";
                        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(uri));
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        startActivity(callIntent);
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), "Your call has failed...",
                                Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                        break;
                    case 3:
                        String uri = "http://www.karibusms.com/";
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(uri));
                        startActivity(intent);
                        break;
                    case 4:
                        showDialog();
                        break;


                    case 5:
                        String url = "http://karibusms.com/privacy";
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);
                        break;
                    case 6:
                        String uriFacebook = "https://www.facebook.com/karibuSMS/";
                        Intent intentFacebook = new Intent(Intent.ACTION_VIEW);
                        intentFacebook.setData(Uri.parse(uriFacebook));
                        startActivity(intentFacebook);
                        break;
                    case 7:
                        String uriTwitter = "https://twitter.com/karibusms";
                        Intent intentTwitter = new Intent(Intent.ACTION_VIEW);
                        intentTwitter.setData(Uri.parse(uriTwitter));
                        startActivity(intentTwitter);
                        break;
                    case 8:

                        String rateUs = "https://play.google.com/store/apps/details?id=com.inets.karibusms&hl=en";
                        Intent IntentRateUs = new Intent(Intent.ACTION_VIEW);
                        IntentRateUs.setData(Uri.parse(rateUs));
                        startActivity(IntentRateUs);
                        break;
                    case 9:
                        Intent shareIntent = new Intent();
                        shareIntent.setAction(Intent.ACTION_SEND);
                        shareIntent.putExtra(Intent.EXTRA_TEXT, "https://goo.gl/ssDOQ0");
                        shareIntent.setType("text/plain");
                        startActivity(Intent.createChooser(shareIntent, "Share karibuSMS via"));
                        break;

                }
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private class MyPhoneListener extends PhoneStateListener {

        private boolean onCall = false;

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

            switch (state) {
                case TelephonyManager.CALL_STATE_RINGING:
                    // phone ringing...
                 /*   Toast.makeText(getActivity(), incomingNumber + " calls you",
                            Toast.LENGTH_LONG).show();*/
                    break;

                case TelephonyManager.CALL_STATE_OFFHOOK:
                    // one call exists that is dialing, active, or on hold
                  /*  Toast.makeText(getActivity(), "on call...",
                            Toast.LENGTH_LONG).show();*/
                    //because user answers the incoming call
                    onCall = true;
                    break;

                case TelephonyManager.CALL_STATE_IDLE:
                    // in initialization of the class and at the end of phone call

                    // detect flag from CALL_STATE_OFFHOOK
                    if (onCall == true) {

                        // restart our application
                        Intent restart = getActivity().getBaseContext().getPackageManager().
                                getLaunchIntentForPackage(getActivity().getBaseContext()
                                        .getPackageName());
                        restart.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(restart);
                        onCall = false;
                    }
                    break;
                default:
                    break;
            }

        }
    }

    public void showDialog() {
        mVolleySingleton = VolleySingleton.getInstance();
        imageLoader = mVolleySingleton.getImageLoader();
        requestQueue = Volley.newRequestQueue(getActivity());
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater(getArguments());
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        dialogBuilder.setView(dialogView);
        final EditText edt = (EditText) dialogView.findViewById(R.id.subscribe);
        final EditText number = (EditText) dialogView.findViewById(R.id.phonenumber);
        number.setVisibility(View.VISIBLE);
        edt.setText("");
        number.setText("");
        edt.setHint("Type your feedback here...");
        number.setHint("Type your phone number here...");
        dialogBuilder.setTitle("Feedback");
        dialogBuilder.setMessage("Thank you for using KaribuSMS.We would like to hear from you," +
                "leave us comment to help improve KaribuSMS");

        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String feedback = edt.getText().toString().trim();
                String phoneno = number.getText().toString().trim();
                if (feedback.isEmpty()) {
                    L.t(getContext(), "Please enter your feedback");
                    return;
                }
                if (phoneno.isEmpty()) {
                    L.t(getActivity(), "Please enter your phone number");
                    return;
                }
                String feedbackurl = AppConfig.feedbackUrl("feedback", feedback, phoneno);
                httpRequest(feedbackurl);
                // L.t(context,"Thank you for your Post");
            }

        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
                dialog.dismiss();
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.setCancelable(false);
        b.show();
    }



    private void  httpRequest(String url){
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseData(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                errormsg=new ErrorMessage(getActivity());
                errormsg.handleVolleyError(error);
            }
        });

// add the request object to the queue to be executed
        requestQueue.add(req);
    }
    //This method will parse json data
    private void parseData(JSONObject response) {
        if (response != null && response.length() > 0) {
            try {
                String status=response.getString("status");
                if(status.equalsIgnoreCase("success")){
                    L.t(getActivity(),response.getString("message"));
                }else {
                    L.t(getActivity(),response.getString("message"));
                }
            }catch (Exception e){

            }
        }

    }
    }

