package karibusms.fragments;

import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import karibusms.R;
import karibusms.extras.AppConfig;
import karibusms.extras.SessionManager;
import karibusms.extras.Utils;
import karibusms.logging.L;

public class UploadContact extends Fragment {

    private RequestQueue requestQueue;
    private   EditText contactNumber;
    List<String> allNumbers;
    private  TextView phonebook;
    String businessname,business_id;
    private String owner_phone;
    Button buttonPickContact;
    ProgressBar progressBar;
    String contact_numbers;
    SessionManager session;
    String firstname;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(karibusms.R.layout.upload_contact, container, false);
        requestQueue= Volley.newRequestQueue(getActivity());
        progressBar = (ProgressBar) rootView.findViewById(R.id.uploadBar);
        buttonPickContact = (Button)rootView.findViewById(R.id.upload);
        contactNumber = (EditText) rootView.findViewById(R.id.phone_no);
        phonebook = (TextView) rootView.findViewById(R.id.phonebook);
        businessname=Utils.readPreferences(getActivity(),"businessname","");
        business_id=Utils.readPreferences(getActivity(),"business_id","");
        L.m(business_id.toString());
        owner_phone=Utils.readPreferences(getActivity(),"phonenumber","");
        session=new SessionManager(getActivity());

        phonebook.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                startActivityForResult(intent,5);


            }});
        buttonPickContact.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                progressBar.setVisibility(View.VISIBLE);
                 contact_numbers=contactNumber.getText().toString().trim();
                if(contact_numbers.length()<9) {
                    L.t(getActivity(),"Please enter the phone number(s) first)");
                    return;
                }
               // L.m(contact_numbers);
//                String url=AppConfig.uploadUrl("storeContacts",contact_numbers,business_id,);
//               httpRequest(url);

            }});

        return rootView;
    }

private void  httpRequest(String url){
    progressBar.setVisibility(View.VISIBLE);
// Post params to be sent to the server
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("tag", "storeContacts");
        params.put("phone_number", contact_numbers);
        params.put("business_id", business_id);

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,url,
                new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                    parseData(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleVolleyError(error);
            }
        });
    requestQueue.add(req);
       }
    public void handleVolleyError(VolleyError error) {
        progressBar.setVisibility(View.GONE);
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            printstatus("Oops! unable to connect.Review your network settings.");
        }
        else if (error instanceof AuthFailureError) {
            printstatus("Oops! KaribuSMS Says It Doesn\'t Recognize You");
            //TODO
        } else if (error instanceof ServerError) {
            printstatus("Oops! KaribuSMS Server Just Messed Up");
            //TODO
        } else if (error instanceof NetworkError) {
            printstatus("Oops! There is network error.Review your network settings.");
            //TODO
        } else if (error instanceof ParseError) {
            printstatus("Oops! Data Received Was Unreadable");
            //TODO
        } else {

        }
    }
    public void printstatus(String msg) {
        new AlertDialog.Builder(getActivity())
                .setTitle("Failure Response")
                .setMessage(msg)
                .setIcon(R.drawable.ic_attachment)
                .setPositiveButton("Retry",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                               dialog.dismiss();
                            }
                        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }
    public void printMsg(String msg) {
        new AlertDialog.Builder(getActivity())
                .setTitle("Info")
                .setMessage(msg)
                .setIcon(R.drawable.ic_attachment)
                .setPositiveButton("Done",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }
    //This method will parse json data
    private void parseData(JSONObject response) {
        progressBar.setVisibility(View.GONE);
        L.m(response.toString());
        if (response != null && response.length() > 0) {
//DO SOMETHING HERE
            try {
                printMsg(response.getString("message"));
       // L.t(getActivity(),response.getString("message"));

            }catch (JSONException e){
                e.printStackTrace();
            }


        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)  {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Cursor cursor = null;
        String phoneNumber = "", primaryMobile = "";
        String displayName="";
        allNumbers = new ArrayList<String>();

        int contactIdColumnId = 0, phoneColumnID = 0, nameColumnID = 0;
        try {
            Uri result = data.getData();
            String id = result.getLastPathSegment();
             cursor = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", new String[] { id }, null);
            contactIdColumnId = cursor.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID);
            phoneColumnID = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA);
            nameColumnID = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);



            if (cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {
                   // idContactBook = cursor.getString(contactIdColumnId);
                    displayName = cursor.getString(nameColumnID);
                    phoneNumber = cursor.getString(phoneColumnID);

                    if (phoneNumber.length() == 0)
                        continue;

                    int type = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                    if (type == ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE && primaryMobile.equals("")) {
                        primaryMobile = phoneNumber;
                       // firstname=displayName;
                    }
                    allNumbers.add(phoneNumber);
                    cursor.moveToNext();
                }
                contactNumber.setText(phoneNumber);
            } else {
                L.t(getActivity(),allNumbers.toString());
                // no results actions
            }
        } catch (Exception e) {
            L.m("SHOWING error");
          e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_main, menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.logout:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
