package karibusms.info;

/**
 * Created by Yohana on 4/26/2016.
 */
public class Business {
    //Data Variables
    private String b_imageUrl;
    private String b_name;
    private  String b_location;
    private  String b_description;
    private String subscriber;
    private String b_id;
    private String phon_number;

    //Getters and Setters
    public String getImageUrl() {
        return b_imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.b_imageUrl = imageUrl;
    }

    public String getName() {
        return b_name;
    }

    public void setName(String name) {
        this.b_name = name;
    }
    public String getPhone() {
        return phon_number;
    }

    public void setPhone(String phon_number) {
        this.phon_number = phon_number;
    }

    public String getId() {
        return b_id;
    }

    public void SetId(String b_id) {
        this.b_id = b_id;
    }

    public String getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(String subscriber) {
        this.subscriber = subscriber;
    }
    public String getLocation() {
        return b_location;
    }

    public void setLocation(String b_location) {
        this.b_location = b_location;
    }
    public String getDescription() {
        return b_description;
    }

    public void setDescription(String b_description) {
        this.b_description = b_description;
    }
}
