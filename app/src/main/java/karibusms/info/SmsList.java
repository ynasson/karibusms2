package karibusms.info;

/**
 * Created by Yohana on 5/5/2016.
 */
public class SmsList {
    private String link;
    private String s_phone;
    private  String b_msg;
    private Integer id;

    //Getters and Setters
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPhone() {
        return s_phone;
    }

    public void setPhone(String s_phone) {
        this.s_phone = s_phone;
    }
//get subscriber id
    public Integer getSid() {
        return id;
    }

    public void setSid(Integer id) {
        this.id = id;
    }
    public String getMsg() {
        return b_msg;
    }

    public void setMsg(String b_msg) {
        this.b_msg = b_msg;
    }
}
