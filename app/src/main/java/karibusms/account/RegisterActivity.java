package karibusms.account;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import karibusms.R;

import karibusms.dashboard.DashboardActivity;
//import karibusms.activities.MainActivity;
import karibusms.extras.AppConfig;
import karibusms.extras.ErrorMessage;
import karibusms.extras.SessionManager;
import karibusms.extras.Utils;
import karibusms.logging.L;

 public class RegisterActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private TextView tvLogin;
    private TextInputLayout businessname;
    private TextInputLayout phoneRegister;
    private TextInputLayout passwordRegister;
    private TextInputLayout passwordConfirm;
    private TextInputLayout emailRegister;
    private EditText etBusinessName;
    private Toolbar toolbar;
    private EditText etPhoneRegister;
    private EditText etPasswordRegister;
    private EditText etconfirm;
    private EditText etEmail;
    private Button registerButton;
    private final Context context=this;
    private String validphone;
    private SessionManager session;
    private ProgressBar progressBar;
    private Spinner spinner;
    private String  business_type;
    private ErrorMessage errormsg;
    private RequestQueue requestQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.karibu_register);
        setUpToolbar();
        initializeViews();
    }

    private  void initializeViews(){
        requestQueue = Volley.newRequestQueue(this);
        spinner=(Spinner)findViewById(R.id.b_type);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.business_type, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        registerButton = (Button) findViewById(R.id.register_button);
        businessname = (TextInputLayout) findViewById(R.id.bname_registerlayout);
        emailRegister = (TextInputLayout) findViewById(R.id.email_registerlayout);
        phoneRegister = (TextInputLayout) findViewById(R.id.phone_registerlayout);
        passwordRegister = (TextInputLayout) findViewById(R.id.password_registerlayout);
        passwordConfirm = (TextInputLayout) findViewById(R.id.confirm_passwordlayout);
        etBusinessName = (EditText) findViewById(R.id.bname_register);
        etPhoneRegister = (EditText) findViewById(R.id.phone_register);
        etPasswordRegister = (EditText) findViewById(R.id.password_register);
        etEmail=(EditText) findViewById(R.id.email_register);
        etconfirm = (EditText) findViewById(R.id.password_confirm);
        tvLogin = (TextView) findViewById(R.id.tv_signin);
        progressBar = (ProgressBar)findViewById(R.id.uploadBar);
        tvLogin.setOnClickListener(this);
        registerButton.setOnClickListener(this);
        session = new SessionManager(getApplicationContext());

        if (session.isLoggedIn()) {
            // User is already logged in. Move to main activity
            Intent intent = new Intent(RegisterActivity.this,
                    DashboardActivity.class);
            startActivity(intent);
            finish();
        }

    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        //L.t(context,""+ parent.getItemAtPosition(pos));
        business_type=parent.getItemAtPosition(pos).toString();
    }


    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }
    private  void setUpToolbar(){
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

    }

    private void registerUser(String url) {
        progressBar.setVisibility(View.VISIBLE);

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                       // session.setLogin(true);
                        parseData(response);

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                errormsg=new ErrorMessage(context);
                errormsg.handleVolleyError(error);
            }}) ;
        requestQueue.add(req);
    }

    //This method will parse json data
    private void parseData(JSONObject response) {
        progressBar.setVisibility(View.GONE);
        String message;
        String status;
        if (response != null && response.length() > 0) {
            try {
                 status = response.getString("status");
                if (status.equalsIgnoreCase("success")) {
                    session=new SessionManager(context);
                    String business_id = response.getString("business_id");
                    String business_name = response.getString("business_name");
                    String phonenumber = response.getString("phone_number");
                    Utils.savePreferences(RegisterActivity.this, "logged_in_id", business_id);
                    Utils.savePreferences(RegisterActivity.this, "owner_number", phonenumber);
                    Utils.savePreferences(RegisterActivity.this, "business_name",business_name);

                    // Launch login activity
                    session.setLogin(true);
                    Intent intent = new Intent(RegisterActivity.this, DashboardActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    message=response.getString("message");
                    L.T(context,message);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    public void handleVolleyError(VolleyError error) {
        progressBar.setVisibility(View.GONE);
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            L.T(context,"Oops! Your Connection Timed Out");
        } else if (error instanceof AuthFailureError) {
            L.T(context,"Oops! KaribuSMS Says It Doesn\\'t Recognize You");
            //TODO
        } else if (error instanceof ServerError) {
            L.t(context,"Oops! KaribuSMS Server Just Messed Up");
            //TODO
        } else if (error instanceof NetworkError) {
            L.T(context,"Oops! There is network error");
            //TODO
        } else if (error instanceof ParseError) {
            L.T(context,"Oops! Data Received Was Unreadable");
            // L.T(context,"Oops! Data Received Was Unreadable");
            //TODO
        }else{
            //If an error occurs that means end of the list has reached
            L.t(context, "No More Items Available");

        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_signin:
                Intent intent = new Intent(getApplicationContext(),
                        LoginActivity.class);
                startActivity(intent);
                finish();
            case R.id.register_button:
                String b_name = etBusinessName.getText().toString();
                String p_number = etPhoneRegister.getText().toString().trim();
                String password = etPasswordRegister.getText().toString();
                String emailaddress = etEmail.getText().toString();
                String confirm_password = etconfirm.getText().toString();
                if (!b_name.isEmpty() && !p_number.isEmpty() && !password.isEmpty()&& password
                        .length()> 4 &&!emailaddress.isEmpty()) {
                     if(p_number.length() < 10 || p_number.length() > 14) {
                         Snackbar.make(v, "Please enter valid phonenumber!", Snackbar.LENGTH_LONG)
                                 .show();
                        return;
                    }else if(!password.equals(confirm_password)){
                         Snackbar.make(v, "Please make sure password and confirm password are " +
                                 "equal!", Snackbar
                                 .LENGTH_LONG)
                                 .show();
                         return;
                     }
                     else if(!Utils.isValidEmail(emailaddress)){
                         Snackbar.make(v, "Please Enter a valid email " , Snackbar
                                 .LENGTH_LONG)
                                 .show();
                         return;
                     }
                     else {
                        validphone = GetCountryZipCode() + p_number.substring(p_number.length() - 9);
                         String gcm_id=Utils.readPreferences(getApplicationContext(),"token","");
                         L.m("URL"+gcm_id);
                         String url=AppConfig.registerUrl("register",validphone,password,b_name,
                                 gcm_id,emailaddress,business_type);
                    registerUser(url);
                     }
                } else {
                    Snackbar.make(v, "Please enter the credentials! and password should be " +
                            "greater than 5 characters",
                            Snackbar
                            .LENGTH_LONG)
                            .show();
                }
                break;
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle item selection

        switch (item.getItemId()) {
            case android.R.id.home:
//
                Intent intent = new Intent(this,LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private String GetCountryZipCode(){
        String CountryID="";
        String CountryZipCode="";

        TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        //getNetworkCountryIso
        CountryID= manager.getSimCountryIso().toUpperCase();
        String[] rl=this.getResources().getStringArray(R.array.CountryCodes);
        for(int i=0;i<rl.length;i++){
            String[] g=rl[i].split(",");
            if(g[1].trim().equals(CountryID.trim())){
                CountryZipCode=g[0];
                break;
            }
        }
        return CountryZipCode;
    }
}
