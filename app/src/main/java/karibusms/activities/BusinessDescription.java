package karibusms.activities;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import org.json.JSONObject;
import karibusms.R;
import karibusms.extras.AppConfig;
import karibusms.extras.ErrorMessage;
import karibusms.extras.Utils;
import karibusms.info.TransitionHelper;
import karibusms.logging.L;
import karibusms.network.VolleySingleton;

/**
 * Created by Yohana on 4/26/2016.
 */
public class BusinessDescription extends AppCompatActivity {
   private  String b_url,b_name,location,b_phone,subscriber_phone,business_id;
    NetworkImageView cover_pic,small_image;
    TextView business_name,b_location,phonenumber;
    Context context=this;

    //Imageloader to load image
    private ImageLoader imageLoader;
    private VolleySingleton mVolleySingleton;
    String validphone;

    //Volley Request Queue
    private RequestQueue requestQueue;
    Button contact,subscribe;
    ErrorMessage errormsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.description_activity);
        mVolleySingleton = VolleySingleton.getInstance();
        imageLoader = mVolleySingleton.getImageLoader();
        requestQueue = Volley.newRequestQueue(this);
        initializeViews();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            b_url = extras.getString("b_url");
           // b_name= extras.getString("b_name");
            location= extras.getString("location");
            b_phone=extras.getString("b_phone");
        }

        L.T(context,"URL"+b_url);
        b_name=Utils.readPreferences(context,"business_name","");
        business_id= Utils.readPreferences(context,"business_id","");
        subscriber_phone= Utils.readPreferences(context,"sub_phone","");
    if(b_url.trim().length()!=0) {
        cover_pic.setImageUrl(b_url, imageLoader);
       small_image.setImageUrl(b_url, imageLoader);
    }
        business_name.setText(b_name);
        b_location.setText(location);
        contact.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(context, ContactBusiness.class);
                transitionTo(intent);
            }});
        subscribe.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View arg0) {
                showDialog();
               // httpRequest(1);
            }});
    }
    public void initializeViews(){
        cover_pic = (NetworkImageView) findViewById(R.id.cover_pic);
        small_image=(NetworkImageView)findViewById(R.id.profile_image);
        business_name=(TextView)findViewById(R.id.b_name);
        b_location=(TextView)findViewById(R.id.b_location);
        phonenumber=(TextView)findViewById(R.id.phonenumber);
        contact=(Button)findViewById(R.id.contact);
        subscribe=(Button)findViewById(R.id.subscribe);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle item selection
        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    //JsonArrayRequest of volley
    private void  httpRequest(String url){
// Post params to be sent to the server
       // String phoneime = ((TelephonyManager) getSystemService(TELEPHONY_SERVICE)) .getDeviceId();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                            parseData(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                errormsg=new ErrorMessage(context);
                errormsg.handleVolleyError(error);
            }
        });

// add the request object to the queue to be executed
        requestQueue.add(req);
    }
    //This method will parse json data
    private void parseData(JSONObject response) {
        L.m(response.toString());
        if (response != null && response.length() > 0) {
try {
    String status=response.getString("status");
    if(status.equalsIgnoreCase("success")){
        L.t(context,response.getString("message"));
    }else{
        L.t(context,response.getString("message"));
    }
}catch (Exception e){

}
        }

    }
    void transitionTo(Intent i) {
        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(this, true);
        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairs);
        startActivity(i, transitionActivityOptions.toBundle());
    }


        public void showDialog() {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
            dialogBuilder.setView(dialogView);

            final EditText edt = (EditText) dialogView.findViewById(R.id.subscribe);
            edt.setText(subscriber_phone);
            edt.setHint("Enter subscription phone number here");
            dialogBuilder.setTitle("You are about to Subscribe");
            dialogBuilder.setMessage("Enter Subscription phone number");
            dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    String s_phone=edt.getText().toString().trim();
                    if (s_phone.length() < 10 || s_phone.length() > 14) {
                      L.t(context,"please enter correct phonenumber");
                        return;
                    }
                    validphone =GetCountryZipCode()+ s_phone.substring(s_phone.length() - 9);
                    Utils.savePreferences(context,"sub_phone",validphone);
                    String phoneime = ((TelephonyManager) getSystemService(TELEPHONY_SERVICE))
                            .getDeviceId();
                    String url=AppConfig.subscribeUrl("subscribe",validphone,phoneime,business_id);
                    httpRequest(url);
                }
            });
            dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //pass
                    dialog.dismiss();
                }
            });
            AlertDialog b = dialogBuilder.create();
            b.show();
        }
    public String GetCountryZipCode(){
        String CountryID="";
        String CountryZipCode="";

        TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        //getNetworkCountryIso
        CountryID= manager.getSimCountryIso().toUpperCase();
        String[] rl=this.getResources().getStringArray(R.array.CountryCodes);
        for(int i=0;i<rl.length;i++){
            String[] g=rl[i].split(",");
            if(g[1].trim().equals(CountryID.trim())){
                CountryZipCode=g[0];
                break;
            }
        }
        return CountryZipCode;
    }
}
