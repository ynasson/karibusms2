package karibusms.activities;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;


import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;



import android.view.View;
import android.widget.ImageView;



import com.android.volley.RequestQueue;
import com.android.volley.Response;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

import karibusms.R;
import karibusms.adapters.bn_adapter;
import karibusms.dashboard.DashboardActivity;
import karibusms.extras.AppConfig;
import karibusms.extras.ErrorMessage;
import karibusms.extras.Utils;
import karibusms.info.Business;
import karibusms.logging.L;
/**
 * Created by Yohana on 6/21/2016.
 */
public class SearchActivity  extends AppCompatActivity implements bn_adapter.clickListener {


    private ArrayList<Business> listBusiness;

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private bn_adapter adapter;

    private RequestQueue requestQueue;
    private ProgressDialog  pdLoading;
    private SearchView searchView;
    private ImageView def_image;

    private int PAGE=1;
    private String url;
    private ErrorMessage errormsg;
    private Context context=this;
    TextView search_text;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);
        initializeSearchView();
// Sets searchable configuration defined in searchable.xml for this SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
        handleIntent(getIntent());
    }
    private void initializeSearchView(){
        //Initializing Views
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
       /// searchView= (SearchView)findViewById(R.id.search);
        def_image=(ImageView)findViewById(R.id.def_image);
        search_text=(TextView)findViewById(R.id.no_search);
        searchView.setSubmitButtonEnabled(true);
        pdLoading=  new ProgressDialog(SearchActivity.this);
        requestQueue = Volley.newRequestQueue(getApplicationContext());
    }
    @Override
    public void onItemClicked(int position) {
        Intent intent = new Intent(getApplicationContext(),Description.class);
        String b_url=listBusiness.get(position).getImageUrl();
        String business_name=listBusiness.get(position).getName();
        String location=listBusiness.get(position).getLocation();
        String business_id=listBusiness.get(position).getId().toString();
        String descript=listBusiness.get(position).getDescription().toString();
        // L.m("Business ID:"+business_id);
        String business_phone=listBusiness.get(position).getPhone().toString();

        // String phonenumber= Utils.readPreferences(getApplicationContext(),"phone_number","");
        Utils.savePreferences(getApplicationContext(),"business_name",business_name);
        Utils.savePreferences(getApplicationContext(),"business_id",business_id);
        Bundle bundle = new Bundle();
        bundle.putString("b_url", b_url);
        bundle.putString("descript", descript);

        // bundle.putString("b_name", business_name);
        bundle.putString("business_id", business_id);
        bundle.putString("location", location);
        bundle.putString("b_phone", business_phone);
        intent.putExtras(bundle);
        startActivity(intent);
    }
    //JsonArrayRequest of volley
    private void httpRequest(String query) {
        pdLoading.setMessage("\tLoading...");
        pdLoading.setCancelable(false);
        pdLoading.show();
        def_image.setVisibility(View.INVISIBLE);

        url = AppConfig.profile("profile",String.valueOf(PAGE));
        //` `   params.put("tag", "profile" );

        url = AppConfig.search("searchProfile",query);
        // params.put("tag", "profile");

        JsonObjectRequest req = new JsonObjectRequest(url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseData(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pdLoading.hide();
                def_image.setVisibility(View.VISIBLE);
                errormsg=new ErrorMessage(context);
                errormsg.handleVolleyError(error);
            }
        });

// add the request object to the queue to be executed
        requestQueue.add(req);
    }
    //This method will parse json data
    private void parseData(JSONObject response) {
        pdLoading.hide();
        adapter = new bn_adapter(SearchActivity.this);
        listBusiness = new ArrayList<>();
        //L.m("JSON OBJECT" + response.toString());
        if (response != null && response.length() > 0) {
          //  L.m("JSON OBJEct" + response.length());
            try {
                JSONArray businessinfo_array = response.getJSONArray("business");
                // L.m("JSON OBJECT"+businessinfo_array.toString());
                if(businessinfo_array.length()>0){
                for (int i = 0; i < businessinfo_array.length(); i++) {
                    Business business_datail = new Business();
                    JSONObject businessinfo = businessinfo_array.getJSONObject(i);
                    if(businessinfo.getString("url").equalsIgnoreCase("null")){
                        business_datail.setImageUrl("");
                    }else {
                        business_datail.setImageUrl(businessinfo.getString("url"));
                    }
                    business_datail.setName(businessinfo.getString("business_name"));
                    if(businessinfo.getString("location").equalsIgnoreCase("null")){
                        business_datail.setLocation("");
                    }else {
                        business_datail.setLocation(businessinfo.getString("location"));
                    }
                    if(businessinfo.getString("description").equalsIgnoreCase("null") || businessinfo.getString("description").isEmpty()){
                        business_datail.setDescription("Description not updated yet");
                    } else {
                        business_datail.setDescription(businessinfo.getString("description"));
                    }
                    business_datail.setSubscriber(businessinfo.getString("subscribers"));
                    business_datail.SetId(businessinfo.getString("business_id"));
                    business_datail.setPhone(businessinfo.getString("phone_number"));
                    listBusiness.add(business_datail);
                }} else {
                    L.t(context,"No data found");
                    adapter.applyAndAnimateRemovals(listBusiness);
                    def_image.setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {

                search_text.setText("No data found");
                adapter.setbusiness(null);
                //recyclerView.setVisibility(View.GONE);
                def_image.setVisibility(View.VISIBLE);
            }
            linearLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(linearLayoutManager);
            search_text.setText(" ");
            adapter.setbusiness(null);
            adapter.setbusiness(listBusiness);
            recyclerView.swapAdapter(adapter,true);
            adapter.setItemClick(this);
            adapter.notifyDataSetChanged();
        }else {
            adapter.setbusiness(null);
            //recyclerView.setVisibility(View.GONE);
           search_text.setText("No data found");
        }
    }
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }
    protected void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            if (searchView != null) {
                searchView.clearFocus();
            }
            httpRequest(query);
        }
        }

    public void backHome(View view) {
        Intent intent = new Intent(SearchActivity.this, DashboardActivity.class);
        finish();
        startActivity(intent);

    }
}
