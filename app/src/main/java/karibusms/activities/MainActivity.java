//package karibusms.activities;
//
//import android.app.Fragment;
//import android.app.FragmentManager;
//
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.net.Uri;
//import android.support.design.widget.NavigationView;
//import android.support.v4.view.GravityCompat;
//import android.support.v4.widget.DrawerLayout;
//import android.support.v7.app.ActionBar;
//import android.os.Bundle;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AppCompatActivity;
//
//import android.support.v7.widget.Toolbar;
//import android.view.LayoutInflater;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.widget.EditText;
//import android.widget.TextView;
//
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.ImageLoader;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.android.volley.toolbox.Volley;
//
//import org.json.JSONObject;
//
//import java.util.List;
//
//import karibusms.account.LoginActivity;
//import karibusms.adapters.bn_adapter;
//import karibusms.extras.AppConfig;
//import karibusms.extras.ErrorMessage;
//import karibusms.extras.SessionManager;
//import karibusms.extras.Utils;
//import karibusms.R;
//import karibusms.fragments.Subscribe;
//
//import de.hdodenhof.circleimageview.CircleImageView;
//import karibusms.fragments.statistics;
//import karibusms.info.Business;
//import karibusms.logging.L;
//import karibusms.network.VolleySingleton;
//
//
//public class MainActivity extends AppCompatActivity {
//
//    private DrawerLayout mDrawerLayout;
//    private NavigationView navigationView;
//    private Context context=this;
//    private TextView username,phonenumber;
//    private CharSequence mTitle;
//    private ImageLoader imageLoader;
//    private VolleySingleton mVolleySingleton;
//    private RequestQueue requestQueue;
//    private String tittle="";
//    private List<Business> businesses;
//    private SessionManager session;
//    private CircleImageView headerimage;
//    private Intent intent;
//    private String profile_pic, user_name,user_phone;
//    private bn_adapter bnAdapter;
//    private ErrorMessage errormsg;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(karibusms.R.layout.activity_main);
//        Toolbar toolbar = (Toolbar) findViewById(karibusms.R.id.toolbar);
//        setSupportActionBar(toolbar);
//        final ActionBar actionBar = getSupportActionBar();
//        actionBar.setHomeAsUpIndicator(karibusms.R.drawable.ic_menu);
//        actionBar.setDisplayHomeAsUpEnabled(true);
//        initializeMainActivityViews();
//        session=new SessionManager(getBaseContext());
//        tittle="Subscribe to Business";
//        updateDisplay(new Subscribe());
//
//        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(MenuItem menuItem) {
//                // Highlight the selected item, update the title, and close the drawer
//                int id=menuItem.getItemId();
//                menuItem.setChecked(true);
//                mDrawerLayout.closeDrawers();
//
//                switch (id) {
//                    case R.id.upload:
//                       if(session.isLoggedIn()) {
//                           tittle="Upload Contacts";
//                           intent = new Intent(getBaseContext(), UploadContactactivity.class);
//                           startActivity(intent);
//                       } else {
//                           infoDialog();
//                    }
//                        break;
//
//                    case R.id.sendsms:
//                        if(session.isLoggedIn()) {
//                           invalidateOptionsMenu();
//                            startActivity(new Intent(MainActivity.this,SendingSMS.class));
//                        }else {
//                            infoDialog();
//                     }
//
//                        break;
//                    case R.id.statistic:
//                        if(session.isLoggedIn()) {
//                            invalidateOptionsMenu();
//                            tittle = "Statistics";
//                            updateDisplay(new statistics());
//                        }else {
//                            infoDialog();
//                        }
//                        break;
//
//                    case R.id.subscribe:
//                        invalidateOptionsMenu();
//                        tittle="Subscribe to Business";
//                        updateDisplay(new Subscribe());
//                        break;
//
//                    case R.id.about:
//                        invalidateOptionsMenu();
//                        tittle="About Us";
//                        startActivity(new Intent(MainActivity.this,About_us.class));
//                        break;
//
//                    case R.id.feedback:
//                        showDialog();
//                        break;
//
//                    case R.id.karibusms:
//                        String karibu_url = "https://web.facebook.com/karibuSMS/?fref=ts";
//                        Intent intent = new Intent(Intent.ACTION_VIEW);
//                        intent.setData(Uri.parse(karibu_url));
//                        startActivity(intent);
//                        break;
//                }
//                return true;
//            }
//        });
//
//    }
//
//    private void initializeMainActivityViews(){
//        mDrawerLayout = (DrawerLayout) findViewById(karibusms.R.id.drawer_layout);
//        navigationView = (NavigationView) findViewById(karibusms.R.id.navigation_view);
//        View hView =  navigationView.getHeaderView(0);
//        username=(TextView)hView.findViewById(R.id.user_name);
//        phonenumber=(TextView)hView.findViewById(R.id.email);
//        profile_pic=Utils.readPreferences(getBaseContext(),"profile_link","");
//        user_name=Utils.readPreferences(getBaseContext(),"business_owner","");
//        user_phone=Utils.readPreferences(getBaseContext(),"owner_number","");
//        username.setText(user_name);
//        phonenumber.setText(user_phone);
//    }
//
////    private void searchBusiness(){
////        Intent searchIntent=getIntent();
////        if(Intent.ACTION_SEARCH.equalsIgnoreCase(searchIntent.getAction())){
////            String query=searchIntent.getStringExtra(SearchManager.QUERY);
////            Toast.makeText(MainActivity.this,query,Toast.LENGTH_LONG).show();
////
////        }
////    }
//
//    public void updateDisplay(Fragment fragment) {
//        FragmentManager fragmentManager = getFragmentManager();
//        fragmentManager.beginTransaction().replace(karibusms.R.id.frame_container, fragment).commit();
//        getSupportActionBar().setTitle(tittle);
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(karibusms.R.menu.menu_main, menu);
////        SearchView searchView= (SearchView) menu.findItem(R.id.menu_search).getActionView();
////        SearchManager searchManager= (SearchManager) getSystemService(SEARCH_SERVICE);
////        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//
//        if(!session.isLoggedIn()) {
//            for (int i = 0; i < menu.size(); i++) {
//
//                menu.getItem(0).setVisible(false);
//            }
//        }
//        return true;
//    }
//
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        int id = item.getItemId();
//        switch (id) {
//            case android.R.id.home:
//                mDrawerLayout.openDrawer(GravityCompat.START);
//                return true;
//            case R.id.logout:
//               // Utils.deletePreferences(getBaseContext());
//                session.setLogin(false);
//                Intent intent = new Intent(getBaseContext(), LoginActivity.class);
//                startActivity(intent);
//                finish();
//                return true;
//            case R.id.menu_search:
//                 intent = new Intent(getBaseContext(), SearchActivity.class);
//                startActivity(intent);
//                return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//    @Override
//    public void setTitle(CharSequence title) {
//        mTitle = title;
//        getActionBar().setTitle(mTitle);
//    }
//    private void  httpRequest(String url){
//        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,url,null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        parseData(response);
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                errormsg=new ErrorMessage(context);
//                errormsg.handleVolleyError(error);
//            }
//        });
//
//// add the request object to the queue to be executed
//        requestQueue.add(req);
//    }
//    //This method will parse json data
//    private void parseData(JSONObject response) {
//        if (response != null && response.length() > 0) {
//            try {
//                String status=response.getString("status");
//                if(status.equalsIgnoreCase("success")){
//                    L.t(context,response.getString("message"));
//                }else {
//                    L.t(context,response.getString("message"));
//                }
//            }catch (Exception e){
//
//            }
//        }
//
//    }
//    public void showDialog() {
//        mVolleySingleton = VolleySingleton.getInstance();
//        imageLoader = mVolleySingleton.getImageLoader();
//        requestQueue = Volley.newRequestQueue(this);
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
//        LayoutInflater inflater = this.getLayoutInflater();
//        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
//        dialogBuilder.setView(dialogView);
//        final EditText edt = (EditText) dialogView.findViewById(R.id.subscribe);
//        final EditText number = (EditText) dialogView.findViewById(R.id.phonenumber);
//        number.setVisibility(View.VISIBLE);
//        edt.setText("");
//        number.setText("");
//        edt.setHint("Type your feedback here...");
//        number.setHint("Type your phone number here...");
//        dialogBuilder.setTitle("Feedback");
//        dialogBuilder.setMessage("Thank you for using KaribuSMS.We would like to hear from you," +
//                "leave us comment to help improve KaribuSMS");
//        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int whichButton) {
//                String feedback=edt.getText().toString().trim();
//                String phoneno=number.getText().toString().trim();
//                if (feedback.isEmpty()) {
//                    L.t(context,"Please enter your feedback");
//                    return;
//                }
//                if (phoneno.isEmpty()) {
//                    L.t(context,"Please enter your phone number");
//                    return;
//                }
//                String feedbackurl=AppConfig.feedbackUrl("feedback",feedback,phoneno);
//                httpRequest(feedbackurl);
//               // L.t(context,"Thank you for your Post");
//            }
//
//        });
//        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int whichButton) {
//                //pass
//                dialog.dismiss();
//            }
//        });
//        AlertDialog b = dialogBuilder.create();
//        b.setCancelable(false);
//        b.show();
//    }
//
//public void infoDialog(){
//    AlertDialog.Builder builder = new AlertDialog.Builder(this);
//// Add the buttonsb
//    builder.setMessage("Please you have to Login first before you perform this activity");
//    builder.setPositiveButton("GO TO LOGIN", new DialogInterface.OnClickListener() {
//        public void onClick(DialogInterface dialog, int id) {
//            // User clicked OK button
//            intent = new Intent(getBaseContext(), LoginActivity.class);
//            startActivity(intent);
//        }
//    });
//    builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
//        public void onClick(DialogInterface dialog, int id) {
//            // User cancelled the dialog
//            dialog.dismiss();
//        }
//    });
//// Set other dialog properties
//// Create the AlertDialog
//    AlertDialog dialog = builder.create();
//    dialog.setCancelable(false);
//    dialog.show();
//}
//
//}
