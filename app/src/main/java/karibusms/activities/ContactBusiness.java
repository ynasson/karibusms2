package karibusms.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import karibusms.R;
import karibusms.extras.AppConfig;
import karibusms.extras.ErrorMessage;
import karibusms.extras.Utils;
import karibusms.logging.L;
import karibusms.network.VolleySingleton;

/**
 * Created by Yohana on 4/27/2016.
 */
public class ContactBusiness  extends AppCompatActivity {
    String b_phone,subscriber_phone,business_id;
    EditText subscriber_message,s_phone;
    TextView b_name;
    String message,business_name;
    Context context=this;
    //Volley Request Queue
    private RequestQueue requestQueue;
    Button submit_request;
    ProgressBar progressBar1;
    String validphone;
    ErrorMessage errormsg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_business_activity);
        requestQueue = Volley.newRequestQueue(this);
        initializeViews();
        subscriber_message.setText("I saw your profile on KaribuSMS. I'd like to talk to you about" +
                " my project.Please reply if interested.");
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_action_cancel);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar. setBackgroundDrawable(new ColorDrawable(Color.parseColor("#53b567")));
        b_phone= Utils.readPreferences(context,"phonenumber","");
        business_id= Utils.readPreferences(context,"business_id","");
       progressBar1=(ProgressBar)findViewById(R.id.progressBar1);
        business_name=Utils.readPreferences(context,"business_name","");
        b_name.setText("To: "+business_name.toUpperCase());
        submit_request.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View arg0) {
                 message=subscriber_message.getText().toString();
                subscriber_phone=s_phone.getText().toString().trim();
                if(message.length()==0){

                }else if(subscriber_phone.length() < 10 || subscriber_phone.length() > 14) {
                    L.t(context,"please enter correct phone number");
                    return;
                }else {
                    validphone = GetCountryZipCode() + subscriber_phone.substring(s_phone.length() - 9);
                    String url=AppConfig.contactBusiness("contact",validphone,message,business_id);
                    httpRequest(url);
                }

            }});
    }
    public void initializeViews(){

        submit_request=(Button)findViewById(R.id.submit_request);
        subscriber_message=(EditText)findViewById(R.id.request);
        s_phone=(EditText)findViewById(R.id.myphone);
        b_name=(TextView)findViewById(R.id.b_name);

    }
    //JsonArrayRequest of volley
    private void  httpRequest(String url){
        progressBar1.setVisibility(View.VISIBLE);
// Post params to be sent to the server
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("message",  message);
        params.put("sub_phone",  validphone);
        params.put("tag", "contact");
        params.put("b_phone",  b_phone);
        params.put("business_id",  business_id);

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,url, new
                JSONObject
                (params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                            parseData(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar1.setVisibility(View.GONE);
                errormsg=new ErrorMessage(context);
                errormsg.handleVolleyError(error);
            }
        });

// add the request object to the queue to be executed
        requestQueue.add(req);
    }
    //This method will parse json data
    private void parseData(JSONObject response) {
        progressBar1.setVisibility(View.GONE);
        if (response != null && response.length() > 0) {
            try {
               L.t(context,response.getString("message"));

            }catch (Exception e){

            }
        }

    }
    public void handleVolleyError(VolleyError error) {
        progressBar1.setVisibility(View.GONE);
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            L.T(context,"Oops! Your Connection Timed Out");
        } else if (error instanceof AuthFailureError) {
            L.T(context,"Oops! KaribuSMS Says It Doesn\\'t Recognize You");
            //TODO
        } else if (error instanceof ServerError) {
            L.t(context,"Oops! KaribuSMS Server Just Messed Up");
            //TODO
        } else if (error instanceof NetworkError) {
            L.T(context,"Oops! There is network error");
            //TODO
        } else if (error instanceof ParseError) {
             L.T(context,"Oops! Data Received Was Unreadable");
            //TODO
        }else{
            //If an error occurs that means end of the list has reached
            L.t(context, "No More Items Available");

        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public String GetCountryZipCode(){
        String CountryID="";
        String CountryZipCode="";

        TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        //getNetworkCountryIso
        CountryID= manager.getSimCountryIso().toUpperCase();
        String[] rl=this.getResources().getStringArray(R.array.CountryCodes);
        for(int i=0;i<rl.length;i++){
            String[] g=rl[i].split(",");
            if(g[1].trim().equals(CountryID.trim())){
                CountryZipCode=g[0];
                break;
            }
        }
        return CountryZipCode;
    }
}
