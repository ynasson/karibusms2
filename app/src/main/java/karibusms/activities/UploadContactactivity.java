package karibusms.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import karibusms.R;
import karibusms.extras.AppConfig;
import karibusms.extras.ErrorMessage;
import karibusms.extras.SessionManager;
import karibusms.extras.Utils;
import karibusms.logging.L;
import phonecontact.Contact;

public class UploadContactactivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private RequestQueue requestQueue;
    private List<String> allNumbers;
    private String businessname,business_id;
    private EditText contactNumber;
    private TextView phonebook,text_select;
    private String owner_phone;
    private Toolbar toolbar;
    private Button buttonPickContact;
    private ProgressBar progressBar;
    private String contact_numbers;
    private SessionManager session;
    private Context context=this;
    private Spinner spinner;
    private ArrayList<String> groups;
    private String b_group="all";
    final int CONTACT_PICK_REQUEST = 1000;
    private ErrorMessage errormsg;
    private JSONObject contactInfo;
    private JSONArray contactArray;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_contact);
        setUpToolBar();
        initializeUploadContactView();

    }
    private void initializeUploadContactView(){
        requestQueue= Volley.newRequestQueue(context);
        contactArray = new JSONArray();
        groups=new ArrayList<>();
        groups.add("all");
        spinner=(Spinner)findViewById(R.id.group_choice);
        text_select=(TextView) findViewById(R.id.group_select);
        spinner.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item,
                groups));
        spinner.setSelection(groups.indexOf("all"));
        spinner.setOnItemSelectedListener(this);
        progressBar = (ProgressBar)findViewById(R.id.uploadBar);
        buttonPickContact = (Button)findViewById(R.id.upload);
        contactNumber = (EditText)findViewById(R.id.phone_no);
        phonebook = (TextView)findViewById(R.id.phonebook);
        businessname=Utils.readPreferences(context,"businessname","");
        business_id=Utils.readPreferences(context,"business_id","");
        phonebook.setOnClickListener(this);
        buttonPickContact.setOnClickListener(this);
        owner_phone=Utils.readPreferences(context,"phonenumber","");
        session=new SessionManager(context);
        String group_url=AppConfig.groupUrl("getGroup",business_id);
        httpRequest(group_url);
    }
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        //L.t(context,""+ parent.getItemAtPosition(pos));
        b_group=parent.getItemAtPosition(pos).toString();
    }
    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.phonebook:
                // TODO Auto-generated method stub
                Intent intentContactPick = new Intent(UploadContactactivity.this,ContactsPickerActivity.class);
                UploadContactactivity.this.startActivityForResult(intentContactPick,CONTACT_PICK_REQUEST);
                break;
            case R.id.upload:
               // progressBar.setVisibility(View.VISIBLE);
                contact_numbers=contactNumber.getText().toString().trim();
                if(contact_numbers.length()<10) {
                    L.t(context,"Please enter the phonenumber(s) first");
                    return;
                }
               // L.m(contact_numbers);
                List<String> contactList = Arrays.asList(contact_numbers.split(","));

                for (int i = 0; i < contactList.size(); i++) {
                    contactInfo=new JSONObject();
                    try {
                        contactInfo.put("phone_number", contactList.get(i));
                        contactInfo.put("name", "");
                        contactArray.put(i,contactInfo);
                        // phoneArr.add(contactInfo);
                    } catch (JSONException e) {

                    }
                   // phoneArr.add(i,Data);
                }
                String url=AppConfig.uploadUrl("sync",contactArray.toString(),business_id,b_group);
               // L.m(url);
                httpRequest(url);
                break;
            default:
                return;

        }

    }
private void  httpRequest(String url){
    progressBar.setVisibility(View.VISIBLE);

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                    parseData(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                errormsg=new ErrorMessage(context);
                errormsg.handleVolleyError(error);
            }
        });
    requestQueue.add(req);
       }

    public void printMsg(String msg) {
        new AlertDialog.Builder(context)
                .setTitle("Info")
                .setMessage(msg)
                .setIcon(R.mipmap.ic_launcher)
                .setPositiveButton("Done",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        }).show();
    }
    //This method will parse json data
    private void parseData(JSONObject response) {
        L.m(response.toString());
        progressBar.setVisibility(View.GONE);
        contactNumber.setText("");
        if (response != null && response.length() > 0) {

            try {
             if   (response.getString("status").equalsIgnoreCase("group")){
                    JSONArray grouparray = response.getJSONArray("groups");
                    // L.m("groups"+grouparray);
                    for (int i = 0; i < grouparray.length(); i++) {
                        JSONObject phones = grouparray.getJSONObject(i);
                        String group = phones.getString("group");
                        groups.add(group);
                    }
                   // groups.add("all");
                    spinner.setAdapter(new ArrayAdapter<>(this,
                            android.R.layout.simple_spinner_dropdown_item,
                            groups));
                 spinner.setVisibility(View.VISIBLE);
                 text_select.setVisibility(View.VISIBLE);

                   // spinner.setSelection(groups.indexOf("all"));

                }else {
                 printMsg(response.getString("message"));
             }
       // L.t(context,response.getString("message"));

            }catch (JSONException e){
                e.printStackTrace();
            }


        }
    }

    @Override
    public void onActivityResult(int requestCode,int resultCode,Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CONTACT_PICK_REQUEST && resultCode == RESULT_OK) {


            ArrayList<Contact> selectedContacts = data.getParcelableArrayListExtra("SelectedContacts");
            for(int i=0;i<selectedContacts.size();i++) {
                contactInfo=new JSONObject();
                try {
                    contactInfo.put("phone_number", selectedContacts.get(i).phone.toString().replaceAll("(?<=\\d) +(?=\\d)", ""));
                    contactInfo.put("name", selectedContacts.get(i).name.toString());
                    contactArray.put(i,contactInfo);
                    //phoneArr.add(contactInfo);
                    Log.d("CONTACT ARRAY",contactArray.toString());
                } catch (JSONException e) {

                }
                //phoneArr.add(i,contactInfo);
            }
           L.m("CONTACT INFO"+" "+contactInfo.toString());
            String url=AppConfig.uploadUrl("sync",contactArray.toString(),business_id,b_group);
             httpRequest(url);
        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void setUpToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);


    }
}
