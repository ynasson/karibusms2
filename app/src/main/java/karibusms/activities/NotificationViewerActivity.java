package karibusms.activities;

import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.RequestQueue;

import karibusms.R;
import karibusms.dashboard.DashboardActivity;
import karibusms.logging.L;
import karibusms.services.ServiceResults;
import me.tatarka.support.job.JobInfo;
import me.tatarka.support.job.JobScheduler;


public class NotificationViewerActivity extends AppCompatActivity {
    private static final long POLL_FREQUENCY = 288000;
    private JobScheduler mJobScheduler;
    //int corresponding to the id of our JobSchedulerService
    private static final int JOB_ID = 100;
    final public static int SEND_SMS = 101;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_viewer);
       // getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        String content=getIntent().getStringExtra("content_to_show");
        ((TextView)findViewById(R.id.tv_content)).setText(content);
        findViewById(R.id.btn_got_it).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.btn_open_app).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(NotificationViewerActivity.this, DashboardActivity.class));
                setupJob();
                finish();;
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }
    private void setupJob() {
        mJobScheduler = JobScheduler.getInstance(getApplicationContext());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //schedule the job after the delay has been elapsed
                buildJob();
            }
        }, 3000);
    }

    private void buildJob() {
        L.T(getApplicationContext(),"Sending Messages please be patient");
        //attach the job ID and the name of the Service that will work in the background
        JobInfo.Builder builder = new JobInfo.Builder(JOB_ID, new ComponentName(getApplicationContext(), ServiceResults
                .class));
        //set periodic polling that needs net connection and works across device reboots
        builder.setPeriodic(POLL_FREQUENCY)
                .setPersisted(false);
        mJobScheduler.schedule(builder.build());
    }

}
