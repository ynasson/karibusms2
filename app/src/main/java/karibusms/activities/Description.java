package karibusms.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.transition.Slide;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import karibusms.R;
import karibusms.extras.AppConfig;
import karibusms.extras.ErrorMessage;
import karibusms.extras.Utils;
import karibusms.info.TransitionHelper;
import karibusms.logging.L;
import karibusms.network.VolleySingleton;

/**
 * Created by Yohana on 5/19/2016.
 */
public class Description  extends AppCompatActivity {

    private static final String EXTRA_IMAGE = "Karibusms";
    private static final String EXTRA_TITLE = "Get your customers connected to you";
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private  String b_url,b_name,location,b_phone,subscriber_phone,business_id, validphone,descript;
    private ImageLoader imageLoader;
    private VolleySingleton mVolleySingleton;

    //Volley Request Queue
    private RequestQueue requestQueue;
    TextView business_name,b_location,phonenumber,more_description;
    Context context=this;
    Button contact,subscribe;
    ImageView image;

    ErrorMessage errormsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initActivityTransitions();
        setContentView(R.layout.business_info);
        initializeViews();
        mVolleySingleton = VolleySingleton.getInstance();
        imageLoader = mVolleySingleton.getImageLoader();
        requestQueue = Volley.newRequestQueue(this);
       // ViewCompat.setTransitionName(findViewById(R.id.app_bar_layout), EXTRA_IMAGE);
       // supportPostponeEnterTransition();

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            b_url = extras.getString("b_url");
            // b_name= extras.getString("b_name");
            location= extras.getString("location");
            b_phone=extras.getString("b_phone");
            descript=extras.getString("descript");
        }
        b_name= Utils.readPreferences(context,"business_name","");
        business_id= Utils.readPreferences(context,"business_id","");
        subscriber_phone= Utils.readPreferences(context,"sub_phone","");
        phonenumber.setText(b_phone);
        business_name.setText(b_name);
        more_description.setText(descript);
        b_location.setText(location);
        contact.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(context, ContactBusiness.class);
                transitionTo(intent);
            }});
        subscribe.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View arg0) {
                showDialog();
                // httpRequest(1);
            }});
       // image.setBackground( getResources().getDrawable(R.drawable.house));
        if(!b_url.equalsIgnoreCase("null")) {
          //  loadImages(b_url, image);
           // image.setMaxWidth(300);
           // image.setMaxHeight(200);
            imageLoader.get(b_url, ImageLoader.getImageListener(image, R.drawable.gradient_black, R
                    .drawable.gradient));
        }
    }
    public void initializeViews(){
        business_name=(TextView)findViewById(R.id.business_name);
        b_location=(TextView)findViewById(R.id.b_location);
        phonenumber=(TextView)findViewById(R.id.phonenumber);
        more_description=(TextView)findViewById(R.id.more_description);
        contact=(Button)findViewById(R.id.contact);
        subscribe=(Button)findViewById(R.id.subscribe);
        image = (ImageView) findViewById(R.id.image);
        String itemTitle = getIntent().getStringExtra(EXTRA_TITLE);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(itemTitle);
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
    }

    @Override public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        try {
            return super.dispatchTouchEvent(motionEvent);
        } catch (NullPointerException e) {
            return false;
        }
    }

    private void initActivityTransitions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Slide transition = new Slide();
            transition.excludeTarget(android.R.id.statusBarBackground, true);
            getWindow().setEnterTransition(transition);
            getWindow().setReturnTransition(transition);
        }
    }

    private void applyPalette(Palette palette) {
        int primaryDark = getResources().getColor(R.color.primary_dark);
        int primary = getResources().getColor(R.color.primary);
        collapsingToolbarLayout.setContentScrimColor(palette.getMutedColor(primary));
        collapsingToolbarLayout.setStatusBarScrimColor(palette.getDarkMutedColor(primaryDark));
       // updateBackground((FloatingActionButton) findViewById(R.id.fab), palette);
        supportStartPostponedEnterTransition();
    }

    private void updateBackground(FloatingActionButton fab, Palette palette) {
        int lightVibrantColor = palette.getLightVibrantColor(getResources().getColor(android.R.color.white));
        int vibrantColor = palette.getVibrantColor(getResources().getColor(R.color.cardview_dark_background));

        fab.setRippleColor(lightVibrantColor);
        fab.setBackgroundTintList(ColorStateList.valueOf(vibrantColor));
    }
    private void loadImages(String urlThumbnail, final ImageView img) {
        if (!urlThumbnail.equals("")) {
            imageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    img.setImageBitmap(response.getBitmap());
                }
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }
    }
    void transitionTo(Intent i) {
        if (Build.VERSION.SDK_INT >= 16) {
            final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(this, true);
            ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairs);
            startActivity(i, transitionActivityOptions.toBundle());
        } else {
            startActivity(i);
        }

    }


    public void showDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.subscribe);
        edt.setText(subscriber_phone);
        dialogBuilder.setTitle("You are about to Subscribe");
        dialogBuilder.setMessage("Enter Subscription phonenunber");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String s_phone=edt.getText().toString().trim();
                if (s_phone.length() < 10 || s_phone.length() > 14) {
                    L.t(context,"please enter correct phonenumber");
                    return;
                }
                validphone =GetCountryZipCode()+ s_phone.substring(s_phone.length() - 9);
                Utils.savePreferences(context,"sub_phone",validphone);
                String phoneime = ((TelephonyManager) getSystemService(TELEPHONY_SERVICE))
                        .getDeviceId();
                String url= AppConfig.subscribeUrl("subscribe",validphone,phoneime,business_id);
                httpRequest(url);
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
                dialog.dismiss();
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }
    public String GetCountryZipCode(){
        String CountryID="";
        String CountryZipCode="";
        TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        //getNetworkCountryIso
        CountryID= manager.getSimCountryIso().toUpperCase();
        String[] rl=this.getResources().getStringArray(R.array.CountryCodes);
        for(int i=0;i<rl.length;i++) {
            String[] g=rl[i].split(",");
            if(g[1].trim().equals(CountryID.trim())){
                CountryZipCode=g[0];
                break;
            }
        }
        return CountryZipCode;
    }
    //JsonArrayRequest of volley
    private void  httpRequest(String url){
// Post params to be sent to the server
        String phoneime = ((TelephonyManager) getSystemService(TELEPHONY_SERVICE))
                .getDeviceId();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseData(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                errormsg=new ErrorMessage(context);
                errormsg.handleVolleyError(error);
            }
        });

// add the request object to the queue to be executed
        requestQueue.add(req);
    }
    //This method will parse json data
    private void parseData(JSONObject response) {
        L.m(response.toString());
        if (response != null && response.length() > 0) {
            try {
                String status=response.getString("status");
                if(status.equalsIgnoreCase("success")){
                    L.t(context,response.getString("message"));
                }else{
                    L.t(context,response.getString("message"));
                }
            }catch (Exception e){

            }
        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}