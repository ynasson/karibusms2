package karibusms.services;

import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import karibusms.R;
import karibusms.activities.NotificationViewerActivity;

public class KaribusmsGCMListenerService extends GcmListenerService {


    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String phones = data.getString("Notice");
        if (phones.equalsIgnoreCase("1")) {
             String message = data.getString("message");
             String title=data.getString("title");
             sendNotification(title,message);
        } else {
            try {
                JSONObject phone_data = new JSONObject(phones);
                parseData(phone_data);
            } catch (JSONException e) {

            }
        }
    }
    /**
     * Create and show a notification containing the received GCM message.
     * @param title the title of the message received
     * @param message GCM message received
     */
    private void sendNotification(String title, String message) {
        Intent intent = new Intent(this, NotificationViewerActivity.class);
        intent.putExtra("content_to_show",message);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 10 , intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(666899, notificationBuilder.build());
    }
    private void parseData(JSONObject response) {
        String msg="";
        String link="";
        String downloadLink="";
        if (response != null && response.length() > 0) {
            try {
                    msg=response.getString("message");
                    link=response.getString("link");
                   String phonenumber = response.getString("phone_number");
                if(link.equals("")){
                    link=downloadLink;
                }

                  sendSMS(phonenumber,msg +'\n' +link);
                  sendNotification("karibusms","messages are sent from www.karibusms.com");
                }
            catch (JSONException e) {
e.                 printStackTrace();
            }
        }
    }
    public void sendSMS(String phoneNumber, String message) {
        SmsManager smsManager = SmsManager.getDefault();
        if(message.length()<160) {
            smsManager.sendTextMessage(phoneNumber, null, message, null, null);
        } else {
            ArrayList<String> parts = smsManager.divideMessage(message);
            smsManager.sendMultipartTextMessage(phoneNumber, null, parts, null,
                    null);
        }
    }

}