package karibusms.extras;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import karibusms.application.MyApplication;
import karibusms.info.SmsList;

/**
 * Created by Yohana on 2/1/2016.
 */
public class Utils {
public  static String deviceName = android.os.Build.MODEL;
    public  static String deviceMan = android.os.Build.MANUFACTURER;
    public static void savePreferences(Context activity, String key, String value){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key, value);
        editor.commit();
    }
    public static void deletePreferences(Context activity){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.commit();
    }
    public static String readPreferences(Context activity, String key, String defaultValue){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        return sp.getString(key, defaultValue);
    }
    public static ArrayList<SmsList> loadPhonenumber() {

            ArrayList<SmsList> listphones= MyApplication.getWritableDatabase().perseSmsinfo();
           return listphones;
    }
    // validating email id
   public static boolean  isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

}
