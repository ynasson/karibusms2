package karibusms.extras;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

import karibusms.fragments.ReceivedSMS;


public class SmsBroadcastReceiver extends BroadcastReceiver {
    public static final String SMS_BUNDLE = "pdus";
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle intentExtras = intent.getExtras();
        if (intentExtras != null) {
            Object[] sms = (Object[]) intentExtras.get(SMS_BUNDLE);
            String smsMessageStr = "";
            if (smsMessageStr.startsWith("Karibu")) {

                for (int i = 0; i < sms.length; ++i) {
                    SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) sms[i]);
                    String smsBody = smsMessage.getMessageBody().toString();
                    String address = smsMessage.getOriginatingAddress();
                    smsMessageStr += "SMS From: " + address + "\n";
                    smsMessageStr += smsBody + "\n";
                }
            }else {
                Toast.makeText(context, "No message", Toast.LENGTH_SHORT).show();
            }

            Toast.makeText(context, smsMessageStr, Toast.LENGTH_SHORT).show();
            //this will update the UI with message
            ReceivedSMS inst = ReceivedSMS.instance();
            inst.updateList(smsMessageStr);
        }
    }

    }

