package karibusms.extras;
import android.net.Uri;



public class AppConfig {

	public static String SERVER_URL = "http://karibusms.com/android_test?";
	public static String TEST_URL = "http://karibusms.com/kiosk/claim.php";

	public static String profile(String tag,String page) {
		String profile_url=SERVER_URL+"tag="+tag+"&page="+page;
		return profile_url;
	}

	public static String loginUrl(String tag,String phonenumber,String password,String gcm_id,
								  String manufacturer,String model){
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http")
				.authority("karibusms.com")
				.appendPath("android_test")
				.appendQueryParameter("tag", tag)
				.appendQueryParameter("phone_number", phonenumber)
				.appendQueryParameter("password", password)
				.appendQueryParameter("gcm_id", gcm_id)
				.appendQueryParameter("manufacturer", manufacturer)
				.appendQueryParameter("model", model);
		String loginUrl = builder.build().toString();
		return loginUrl;
	}
	public static String registerUrl(String tag,String phonenumber,String password,String
			business_name,String gcm_id,String email,String business_type){
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http")
				.authority("karibusms.com")
				.appendPath("android_test")
				.appendQueryParameter("tag", tag)
				.appendQueryParameter("phone_number", phonenumber)
				.appendQueryParameter("password", password)
				.appendQueryParameter("business_name", business_name)
				.appendQueryParameter("gcm_id", gcm_id)
				.appendQueryParameter("email", email)
				.appendQueryParameter("business_type", business_type);
		String register_url = builder.build().toString();
		return register_url;
	}
	public static String subscribeUrl(String tag,String phonenumber,String phoneime,String
			business_id){
		String subscribe_url=SERVER_URL+"tag="+tag+"&sub_phone="+phonenumber+"&phoneime" +
				"="+phoneime+"&business_id="+business_id;
		return subscribe_url;
	}
	public static String contactBusiness(String tag,String phonenumber,String message,String
			business_id){
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http")
                .authority("karibusms.com")
                .appendPath("android_test")
                .appendQueryParameter("tag", tag)
                .appendQueryParameter("sub_phone", phonenumber)
                .appendQueryParameter("message", message)
                .appendQueryParameter("business_id", business_id);
        String contact_url = builder.build().toString();
        return  contact_url;
	}
	public static String uploadUrl(String tag,String contact,String
			business_id,String category_name){
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http")
				.authority("karibusms.com")
				.appendPath("android_test")
				.appendQueryParameter("tag", tag)
				.appendQueryParameter("contacts", contact)
				.appendQueryParameter("business_id", business_id)
				.appendQueryParameter("category_name", category_name);
		String upload_url=builder.build().toString();
		return upload_url;
	}
	public static String sendsmsUrl(String tag,String business_id,String content,String
			category_name,String message_type){
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http")
				.authority("karibusms.com")
				.appendPath("android_test")
				.appendQueryParameter("tag", tag)
				.appendQueryParameter("business_id", business_id)
				.appendQueryParameter("content", content)
				.appendQueryParameter("category_name", category_name)
				.appendQueryParameter("message_type", message_type);
		String sendsms_url = builder.build().toString();
		return sendsms_url;
	}
	public static String sendSingleSMSUrl(String tag,String business_id,String phone_number,String content,String message_type)
	{
		 Uri.Builder builder=new Uri.Builder();
		 builder.scheme("http")
				 .authority("karibusms.com")
				 .appendPath("android_test")
				 .appendQueryParameter("tag", tag)
				 .appendQueryParameter("business_id", business_id)
                 .appendQueryParameter("phone_numbers",phone_number)
				 .appendQueryParameter("content", content)
				 .appendQueryParameter("message_type", message_type);
		String sendSignleUrl=builder.build().toString();
		return sendSignleUrl;

	}

	public static String statisticUrl(String tag,String business_id){
		String statistic_url=SERVER_URL+"tag="+tag+"&business_id="+business_id;
		return statistic_url;
	}
    public static String groupUrl(String tag,String business_id){
        String group_url=SERVER_URL+"tag="+tag+"&business_id="+business_id;
        return group_url;
    }
    public static String feedbackUrl(String tag,String message,String phonenumber){
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http")
				.authority("karibusms.com")
				.appendPath("android_test")
				.appendQueryParameter("tag", tag)
				.appendQueryParameter("message", message)
		       .appendQueryParameter("phone_number", phonenumber);
		String feedback_url = builder.build().toString();
		return  feedback_url;
    }
	public static String search(String tag,String name){
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http")
				.authority("karibusms.com")
				.appendPath("android_test")
				.appendQueryParameter("tag", tag)
				.appendQueryParameter("name",name);
		String search_url = builder.build().toString();
		return  search_url;
	}
	public static String faqUrl(String status) {

		String faqurl=SERVER_URL+"tag="+status;

		return  faqurl;
	}

	public static String sentSMSUrl(String tag,String business_id){
		Uri.Builder builder=new Uri.Builder();
		builder.scheme("http")
				.authority("karibusms.com")
				.appendPath("android_test")
				.appendQueryParameter("tag",tag)
				.appendQueryParameter("business_id",business_id);
		String sent_sms_url=builder.build().toString();
		return sent_sms_url;

	}

	public static String userProfile(String tag,String business_id){
		Uri.Builder builder=new Uri.Builder();
		builder .scheme("http")
				.authority("karibusms.com")
				.appendPath("android_test")
				.appendQueryParameter("tag",tag)
				.appendQueryParameter("business_id",business_id);
		String user_profile_url=builder.build().toString();
		return user_profile_url;
	}

	public static String getContacts(String tag,String business_id){
		Uri.Builder builder=new Uri.Builder();
		builder.scheme("http")
				.authority("karibusms.com")
				.appendPath("android_test")
				.appendQueryParameter("tag",tag)
				.appendQueryParameter("business_id",business_id);
		String get_contacts_url=builder.build().toString();
		return get_contacts_url;
	}

	public static String sendDeleteMessageUrl(String tag,String business_id,String message_id){
		Uri.Builder builder=new Uri.Builder();
		builder.scheme("http")
				.authority("karibusms.com")
				.appendPath("android_test")
				.appendQueryParameter("tag",tag)
				.appendQueryParameter("business_id",business_id)
		        .appendQueryParameter("message_id",message_id);
		String send_delete_sms=builder.build().toString();
		return send_delete_sms;
	}
	public static String addCustomContactUrl(String tag,String name,String email,String phone_number,String business_id,String category_name){
		Uri.Builder builder=new Uri.Builder();
		builder.scheme("http")
				.authority("karibusms.com")
				.appendPath("android_test")
				.appendQueryParameter("tag",tag)
				.appendQueryParameter("name",name)
				.appendQueryParameter("email",email)
		        .appendQueryParameter("phone_number",phone_number)
		        .appendQueryParameter("business_id",business_id)
		        .appendQueryParameter("category_name",category_name);
		String sendAddContactUrl=builder.build().toString();
		return sendAddContactUrl;
	}
	public static String createNewGroupUrl(String tag,String name,String business_id){
		Uri.Builder builder=new Uri.Builder();
		builder.scheme("http")
				.authority("karibusms.com")
				.appendPath("android_test")
				.appendQueryParameter("tag",tag)
				.appendQueryParameter("name",name)
				.appendQueryParameter("business_id",business_id);
		String groupUrl=builder.build().toString();
		return groupUrl;
	}

	public static String getCategoriesUrl(String tag,String business_id){
		Uri.Builder builder=new Uri.Builder();
		builder.scheme("http")
				.authority("karibusms.com")
				.appendPath("android_test")
				.appendQueryParameter("tag",tag)
				.appendQueryParameter("business_id",business_id);
		String categoryUrl=builder.build().toString();
		return categoryUrl;
	}
	public static String getContactByCategory(String tag,String business_id,String category_id){
		Uri.Builder builder=new Uri.Builder();
		builder.scheme("http")
				.authority("karibusms.com")
				.appendPath("android_test")
				.appendQueryParameter("tag",tag)
				.appendQueryParameter("business_id",business_id)
				.appendQueryParameter("category_id",category_id);
		String contactByCategoryUrl=builder.build().toString();
		return contactByCategoryUrl;

	}
	public static String deleteCategoryUrl(String tag,String category,String category_id,String business_id){
		Uri.Builder builder=new Uri.Builder();
		builder.scheme("http")
				.authority("karibusms.com")
				.appendPath("android_test")
				.appendQueryParameter("tag",tag)
				.appendQueryParameter("type",category)
				.appendQueryParameter("category_id",category_id)
		        .appendQueryParameter("business_id",business_id);
		String deleteCatUrl=builder.build().toString();
		return deleteCatUrl;

	}
	public static String deleteContactUrl(String tag,String subscriber_info_id,String business_id){
		Uri.Builder builder=new Uri.Builder();
		builder.scheme("http")
				.authority("karibusms.com")
				.appendPath("android_test")
				.appendQueryParameter("tag",tag)
				.appendQueryParameter("subscriber_info_id",subscriber_info_id)
				.appendQueryParameter("business_id",business_id);
		String deleteContUrl=builder.build().toString();
		return deleteContUrl;

	}

}
