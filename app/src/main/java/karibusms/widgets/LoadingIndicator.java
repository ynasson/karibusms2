package karibusms.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.pnikosis.materialishprogress.ProgressWheel;

import karibusms.R;

/**
 * class={@LoadingIndicator} shows a progress bar by default,
 * contains methods that can hide the progress bar and show failure massage
 * and the retry button(optional)
 */
public class LoadingIndicator extends FrameLayout{

    private View retryBtn;
    private TextView msgTv;
    private ProgressWheel progressBar;

    public LoadingIndicator(Context context) {
        super(context);
        init();
    }

    public LoadingIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LoadingIndicator(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init(){
        LayoutInflater.from(getContext()).inflate(R.layout.loading_indicator,this,true);
        retryBtn=findViewById(R.id.btn_refresh);
        msgTv=(TextView)findViewById(R.id.tv_msg);
        progressBar= (ProgressWheel) findViewById(R.id.progress_bar);
        hide();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


    }

    public void show(){
        this.setVisibility(View.VISIBLE);
        showProgressBar();
    }

    private void showProgressBar() {
        msgTv.setVisibility(View.GONE);
        retryBtn.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void showErrorMsg(String msg){
        this.setVisibility(View.VISIBLE);
        retryBtn.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        msgTv.setVisibility(View.VISIBLE);
        msgTv.setText(msg);
    }

    public void showNoItemsMsg(String msg){
        this.setVisibility(View.VISIBLE);
        retryBtn.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.GONE);
        msgTv.setVisibility(View.VISIBLE);
        msgTv.setText(msg);
    }

    public void hide(){
        this.setVisibility(View.GONE);
    }

    public void onLoadError(String msg, final OnLoadErrorListener olel){
        showErrorMsg(msg);
        retryBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (olel!=null){
                    showProgressBar();
                    olel.onRetry();
                }
            }
        });
    }

    public ProgressWheel getRim(){
        return progressBar;
    }

    public void setDominantColor(int themeColor) {
        progressBar.setLinearProgress(true);
        progressBar.setBarColor(themeColor);
        msgTv.setTextColor(themeColor);
        ((ImageView)retryBtn).setColorFilter(themeColor);
    }

    public interface OnLoadErrorListener {
        void onRetry();
    }

}
