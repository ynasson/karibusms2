package karibusms.widgets;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import karibusms.R;


public class FooterLoadingIndicator extends FrameLayout {
	Context con;
    boolean MSG_VISIBLE=false;

	public FooterLoadingIndicator(Context context) {
		super(context);
		con=context;
		init();
	}

	public FooterLoadingIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
		con=context;
		init();
	}

	public FooterLoadingIndicator(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		con=context;
		init();
	}

	private void init() {
		inflate(con, R.layout.footer_loading_indicator, this);
        hide();

	}

    public void hide(){
        MSG_VISIBLE=false;
        this.setVisibility(View.GONE);
    }

    public void showLoader(){
        this.setVisibility(View.VISIBLE);
        this.findViewById(R.id.ll_loading).setVisibility(View.VISIBLE);
        this.findViewById(R.id.ll_reload).setVisibility(View.GONE);
        this.findViewById(R.id.ll_msg).setVisibility(View.GONE);
        this.setOnClickListener(null);
    }

    public void showReload(final OnReload orl){
        this.setVisibility(View.VISIBLE);
        this.findViewById(R.id.ll_loading).setVisibility(View.GONE);
        this.findViewById(R.id.ll_reload).setVisibility(View.VISIBLE);
        this.findViewById(R.id.ll_msg).setVisibility(View.GONE);
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                orl.reLoad();
            }
        });
    }

    public void showMsg(String msg){
        if (!MSG_VISIBLE){
            this.setVisibility(View.VISIBLE);
            this.findViewById(R.id.ll_loading).setVisibility(View.GONE);
            this.findViewById(R.id.ll_reload).setVisibility(View.GONE);
            this.findViewById(R.id.ll_msg).setVisibility(View.VISIBLE);
            ((TextView)this.findViewById(R.id.tv_msg)).setText(msg);
            this.setOnClickListener(null);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    hide();
                }
            },4000);
            MSG_VISIBLE=true;
        }
    }

    public interface OnReload{
        void reLoad();
    }
}
