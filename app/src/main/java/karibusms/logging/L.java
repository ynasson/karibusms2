package karibusms.logging;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Yohana on 1/28/2016.
 */
public class L {
    public static void m(String message) {
        Log.d("KIOSK", "" + message);
    }
    public static void t(Context context, String message) {
        Toast.makeText(context, message + "", Toast.LENGTH_SHORT).show();
    }
    public static void T(Context context, String message) {
        Toast.makeText(context, message + "", Toast.LENGTH_LONG).show();
    }
}
